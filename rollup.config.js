import babel from 'rollup-plugin-babel'
import commonjs from 'rollup-plugin-commonjs'
import json from 'rollup-plugin-json'
import resolve from 'rollup-plugin-node-resolve'
import pkg from './package.json'


const isDevelopment = process.env.NODE_ENV ? process.env.NODE_ENV !== 'production' : true


export default [
	{
		entry: 'src/WikiquoteApi.js',
		dest: pkg.browser,
		format: 'umd',
		moduleName: 'WikiquoteApi',
		sourceMap: true,
		plugins: [
			resolve({
				browser: true,
				jsnext: true,
			}),
			json({
				preferConst: true,
			}),
			commonjs(),
			babel({
				exclude: 'node_modules/**',
			}),
		],
	},

	{
		entry: 'src/WikiquoteApi.js',
		targets: [
			{ dest: pkg.main, format: 'cjs' },
			{ dest: pkg.module, format: 'es' },
		],
		sourceMap: true,
		plugins: [
			resolve({
				jsnext: true,
			}),
			json({
				preferConst: true,
			}),
			commonjs(),
			babel({
				exclude: 'node_modules/**',
			}),
		],
	},
]
