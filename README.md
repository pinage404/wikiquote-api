# Wikiquote API

[![Pipeline status][pipeline_badge]][pipeline_url]
[![Published on NPM][npm_badge]][npm_url]

Small APi for [Wikiquote][wikiquote]


- [How to install](#how-to-install)
- [How to use](#how-to-use)
    - [Import library](#import-library)
        - [ES2017 Module style](#es2017-module-style)
            - [In browser](#in-browser)
            - [In Node](#in-node)
        - [Old style](#old-style)
            - [Old style in browser](#old-style-in-browser)
            - [Old style in Node](#old-style-in-node)
    - [API](#api)
    - [Examples](#examples)
        - [Get all pages tile related to a given subject](#get-all-pages-tile-related-to-a-given-subject)
        - [Get HTML content for a given page title](#get-html-content-for-a-given-page-title)
        - [Get all content as `HTMLElement` for a given subject](#get-all-content-as-htmlelement-for-a-given-subject)
- [Powered by Wikidata](#powered-by-wikidata)
- [Yet another Wikidata API](#yet-another-wikidata-api)
- [License](#license)


## How to install

With [`Yarn`][yarn]

```bash
yarn add wikiquote-api
```

or with `NPM`

```bash
npm install wikiquote-api --save
```


## How to use

### Import library

#### ES2017 Module style

##### In browser

```html
<script type="text/javascript" src="./node_modules/babel-polyfill/dist/polyfill.js"></script>
<script type="module" src="./node_modules/wikiquote-api/dist/WikiquoteApi.module.mjs"></script>
```

##### In Node

```javascript
import 'babel-polyfill'
import WikiquoteApi from './node_modules/wikiquote-api/dist/WikiquoteApi.module.mjs'
```


#### Old style

##### Old style in browser

```html
<script type="text/javascript" src="./node_modules/babel-polyfill/dist/polyfill.js"></script>
<script type="text/javascript" src="./node_modules/wikiquote-api/dist/WikiquoteApi.browser.js"></script>
```

##### Old style in Node

```javascript
require('babel-polyfill')
const WikiquoteApi = require('wikiquote-api')
```


### API

`WikiquoteApi` provides this functions

```typescript
async function searchQuotePage(
	searchTerm: string,
	lang?: string,
	searchUrlConfig?: (lang: string) => string
): string[];

async function getQuotePage(
	pageTitle: string,
	lang?: string,
	pageUrlConfig?: (lang: string) => string,
	ambiguousPageConfig?: (lang: string) => string
): string;

async function getQuotePageDOM(
	pageTitle: string,
	lang?: string,
	pageUrlConfig?: (lang: string) => string,
	ambiguousPageConfig?: (lang: string) => string
): HTMLElement;

async function getFullQuotePage(
	searchTerm: string,
	lang?: string,
	searchUrlConfig?: (lang: string) => string,
	pageUrlConfig?: (lang: string) => string,
	ambiguousPageConfig?: (lang: string) => string
): Map<string, string>;

async function getFullQuotePageDOM(
	searchTerm: string,
	lang?: string,
	searchUrlConfig?: (lang: string) => string,
	pageUrlConfig?: (lang: string) => string,
	ambiguousPageConfig?: (lang: string) => string
): Map<string, HTMLElement>;
```

And this exceptions under the scope `exception`

- `WikiquoteApiException`
- `UnsupportedLanguageException`
- `NoSuchPageException`
- `DisambiguationPageException`


### Examples

#### Get all pages tile related to a given subject

```javascript
// returns a promise that resolve with all Spiderman related pages title
WikiquoteApi.searchQuotePage('Spiderman').then(console.log)
/*
Output:
[ 'Spider-Man',
  'Spider-Man 3',
  'Spider-Man 2',
  'Spider-Man: The Animated Series',
  'Spider-Man (2002 film)',
  'Spider-Man: Homecoming',
  'Spider-Man (2000 video game)',
  'Spider-Man: Shattered Dimensions',
  'Spider-Man 2: Enter Electro',
  'Spider-Man: Web of Shadows',
  'Spider-Man: Edge of Time',
  'Spider-Man 3 (video game)',
  'Spider-Man 2 (Video Game)' ]
*/
```


#### Get HTML content for a given page title

```javascript
// returns a promise that resolve with a HTMLElement with the Spider-Man (2002 film)'s HTML content
WikiquoteApi.getQuotePage('Spider-Man (2002 film)').then(console.log)
/*
Output:
<div class="mw-parser-output"><p><i><b><a href="https://en.wikipedia.org/wiki/Spider-Man_(2002_film)" class="extiw" title="w:Spider-Man (2002 film)">Spider-Man</a></b></i> is a 2002 film based on the eponymous <a href="https://en.wikipedia.org/wiki/Marvel_Comics" class="extiw" title="w:Marvel Comics">Marvel comic</a>. It stars <a href="https://en.wikipedia.org/wiki/Tobey_Maguire" class="extiw" title="w:Tobey Maguire">Tobey Maguire</a> and <a href="https://en.wikipedia.org/wiki/Willem_Dafoe" class="extiw" title="w:Willem Dafoe">Willem Dafoe</a> and explains the story ofPeter Parker's struggle to balance between his normal life and his life as Spider Man. This movie is released theaters May 3, 2002 in United States.</p>
...
<li><i>(last words of the movie)</i> Whatever life holds in store for me, I will never forget these words: "With great power comes great responsibility." This is my gift, my curse. Who am I? I'm Spider-Man.</li>
...
*/
```


#### Get all content as `HTMLElement` for a given subject

```javascript
// returns a promise that resolve with a Map<page's URL, HTMLElement with the content> of each pages related to Spiderman
WikiquoteApi.getFullQuotePageDOM('Iron Man').then(console.log)
/*
Output:
Map {
  'https://en.wikiquote.org/wiki/Iron%20Man%20(2008%20film)' => HTMLDivElement {},
  'https://en.wikiquote.org/wiki/Iron%20Man%3A%20Armored%20Adventures' => HTMLDivElement {},
  'https://en.wikiquote.org/wiki/Iron%20Man%20(comics)' => HTMLDivElement {},
  'https://en.wikiquote.org/wiki/Iron%20Man%203' => HTMLDivElement {},
  'https://en.wikiquote.org/wiki/Iron%20Man%202' => HTMLDivElement {},
  'https://en.wikiquote.org/wiki/Ted%20Hughes' => HTMLDivElement {},
  'https://en.wikiquote.org/wiki/Iron%20Maiden' => HTMLDivElement {} }
*/
```


## Powered by Wikidata

Don't forget to add `Powered by Wikidata` to your project

[![Powered by Wikidata][powered_by_wikidata_img]][powered_by_wikidata_url]


## Yet another Wikidata API

After tried severals [Javascript API client for Wikidata][wikidata_js_client_url] nothing really suits to my needs or was too big :
- too many dependencies
- too many features not used

[![One more comic][commitstrip_one_more_img]][commitstrip_one_more_url]

[![Standards comic][xkcd_standards_img]][xkcd_standards_url]

So, i created this small API for [Wikiquote][wikiquote]


## License

[MIT][license]



[pipeline_badge]: https://gitlab.com/pinage404/wikiquote-api/badges/master/pipeline.svg
[pipeline_url]:   https://gitlab.com/pinage404/wikiquote-api/commits/master

[npm_badge]: https://img.shields.io/npm/v/wikiquote-api.svg
[npm_url]:   https://www.npmjs.com/package/wikiquote-api

[wikiquote]: https://www.wikiquote.org

[yarn]: https://yarnpkg.com

[powered_by_wikidata_img]: https://upload.wikimedia.org/wikipedia/commons/thumb/a/ae/Wikidata_Stamp_Rec_Dark.svg/640px-Wikidata_Stamp_Rec_Dark.svg.png
[powered_by_wikidata_url]: https://www.wikidata.org/wiki/Wikidata:Data_access#Best_practices_to_follow

[wikidata_js_client_url]: https://www.mediawiki.org/wiki/API:Client_code#JavaScript

[commitstrip_one_more_img]: https://www.commitstrip.com/wp-content/uploads/2017/03/Strip-X%C3%A8me-plugin-open-source-english650-final.jpg
[commitstrip_one_more_url]: http://www.commitstrip.com/en/2017/03/22/one-more/

[xkcd_standards_img]: https://imgs.xkcd.com/comics/standards.png
[xkcd_standards_url]: https://xkcd.com/927/

[license]: https://gitlab.com/pinage404/wikiquote-api/blob/master/LICENSE
