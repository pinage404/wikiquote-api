import { UnsupportedLanguageException } from './exception'



const SUPPORTED_LANGUAGES = [
	'en',
	'fr',
]


export default function checkLang(lang='en') {
	if (!SUPPORTED_LANGUAGES.includes(lang)) {
		throw new UnsupportedLanguageException(lang)
	}
}
