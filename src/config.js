export const BASE_URL = lang => `https://${lang}.wikiquote.org/`
export const USER_URL = lang => `${BASE_URL(lang)}wiki/`
// "origin=*" allow CORS request
export const BASE_API_URL = lang => `${BASE_URL(lang)}w/api.php?origin=*`
// "limit" is set to get all responses, without that, we get truncated response
export const SEARCH_URL = lang => `${BASE_API_URL(lang)}&format=json&action=opensearch&redirects=resolve&limit=max&search=`
// "categories" is needed to determine if the page is an disambiguation page
export const PAGE_URL = lang => `${BASE_API_URL(lang)}&format=json&action=parse&prop=text|categories&disabletoc&page=`


export const AMBIGUOUS_PAGE = lang => {
	if (lang === 'fr') {
		return 'Homonymie'
	}

	return 'Disambiguation_pages'
}
