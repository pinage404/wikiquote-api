export default function stringToDOM(html) {
	const doc = document.createDocumentFragment()
	const div = document.createElement('div')
	div.innerHTML = html
	return div
}
