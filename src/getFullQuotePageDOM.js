import stringToDOM from './stringToDOM'
import getFullQuotePage from './getFullQuotePage'



export default async function getFullQuotePageDOM(...arg) {
	const pageMapDOM = new Map
	const pageMapText = await getFullQuotePage(...arg)
	for (const [ pageUrl, pageTextContent ] of pageMapText.entries()) {
		const pageDOMContent = stringToDOM(pageTextContent)
		pageMapDOM.set(pageUrl, pageDOMContent)
	}
	return pageMapDOM
}
