import { SEARCH_URL, PAGE_URL, AMBIGUOUS_PAGE, USER_URL } from './config'
import searchQuotePage from './searchQuotePage'
import getQuotePage from './getQuotePage'



export default async function getFullQuotePage(searchTerm, lang='en', searchUrlConfig=SEARCH_URL, pageUrlConfig=PAGE_URL, ambiguousPageConfig=AMBIGUOUS_PAGE) {
	const pageMap = new Map

	const pageList = await searchQuotePage(searchTerm, lang, searchUrlConfig)
	await Promise.all(pageList.map(
		async pageTitle => {
			const pageContent = await getQuotePage(pageTitle, lang, pageUrlConfig, ambiguousPageConfig)
			const pageUrl = `${USER_URL(lang)}${encodeURIComponent(pageTitle)}`
			pageMap.set(pageUrl, pageContent)
		}
	))

	return pageMap
}
