import stringToDOM from './stringToDOM'
import getQuotePage from './getQuotePage'



export default async function getQuotePageDOM(...arg) {
	const htmlContent = await getQuotePage(...arg)
	const tree = stringToDOM(htmlContent)
	return tree
}
