import checkLang from './checkLang'
import { SEARCH_URL } from './config'
import jsonFromUrl from './jsonFromURL'



export default async function searchQuotePage(searchTerm, lang='en', searchUrlConfig=SEARCH_URL) {
	if (!searchTerm) return []

	checkLang(lang)

	const searchUrl = searchUrlConfig(lang)
	const data = await jsonFromUrl(searchUrl, searchTerm)
	const pageList = data[1]
	return pageList
}
