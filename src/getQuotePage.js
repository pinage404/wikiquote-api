import checkLang from './checkLang'
import { PAGE_URL, AMBIGUOUS_PAGE } from './config'
import jsonFromURL from './jsonFromURL'
import { NoSuchPageException, DisambiguationPageException } from './exception'



function isDisambiguation(categories, ambiguousKeyword) {
	return !categories || categories.some(category => category['*'] === ambiguousKeyword)
}


export default async function getQuotePage(pageTitle, lang='en', pageUrlConfig=PAGE_URL, ambiguousPageConfig=AMBIGUOUS_PAGE) {
	if (!pageTitle) return ''

	checkLang(lang)

	const pageUrl = pageUrlConfig(lang)
	const data = await jsonFromURL(pageUrl, pageTitle)

	if ('error' in data) {
		throw new NoSuchPageException(pageTitle)
	}

	const ambiguousKeyword = ambiguousPageConfig(lang)
	if (isDisambiguation(data.parse.categories, ambiguousKeyword)) {
		throw new DisambiguationPageException(pageTitle)
	}

	const htmlContent = data.parse.text['*']
	return htmlContent
}
