import ExtendableError from 'es6-error'



export class WikiquoteApiException extends ExtendableError {}


export class UnsupportedLanguageException extends WikiquoteApiException {
	constructor(lang, ...arg) {
		const message = `Unsupported language: “${lang}”`
		super(message, ...arg)
	}
}


export class NoSuchPageException extends WikiquoteApiException {
	constructor(pageTitle, ...arg) {
		const message = `No pages matched the title: “${pageTitle}”`
		super(message, ...arg)
	}
}


export class DisambiguationPageException extends WikiquoteApiException {
	constructor(pageTitle, ...arg) {
		const message = `Disambiguation page: “${pageTitle}”`
		super(message, ...arg)
	}
}



export default {
	WikiquoteApiException,
	UnsupportedLanguageException,
	NoSuchPageException,
	DisambiguationPageException,
}
