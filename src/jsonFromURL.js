import 'isomorphic-fetch'


export default async function jsonFromURL(url, param=null) {
	if (param) {
		url += encodeURIComponent(param)
	}

	const option = {
		method: 'GET',
		mode: 'cors',
		credentials: 'omit',
	}

	const res = await fetch(url, option)
	const json = await res.json()
	return json
}
