import exception from './exception'
import searchQuotePage from './searchQuotePage'
import getQuotePage from './getQuotePage'
import getQuotePageDOM from './getQuotePageDOM'
import getFullQuotePage from './getFullQuotePage'
import getFullQuotePageDOM from './getFullQuotePageDOM'



const WikiquoteApi = {
	exception,
	searchQuotePage,
	getQuotePage,
	getQuotePageDOM,
	getFullQuotePage,
	getFullQuotePageDOM,
}

export default WikiquoteApi

export { default as exception } from './exception'
export { default as searchQuotePage } from './searchQuotePage'
export { default as getQuotePage } from './getQuotePage'
export { default as getQuotePageDOM } from './getQuotePageDOM'
export { default as getFullQuotePage } from './getFullQuotePage'
export { default as getFullQuotePageDOM } from './getFullQuotePageDOM'
