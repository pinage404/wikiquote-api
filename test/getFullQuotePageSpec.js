import chai from 'chai'
chai.should()

import fetchMocked from 'fetch-mock'


import isFrozenEnvironment from './isFrozenEnvironment'
import fakeResponse from './fakeResponse'


import getFullQuotePage from '../src/getFullQuotePage'



describe('Wikiquote API get full quote page', function() {
	this.timeout(isFrozenEnvironment ? 300 : 3000)

	if (isFrozenEnvironment) {
		beforeEach(() => {
			fetchMocked.getOnce(/fr\.wikiquote\.org.+&search=Kaamelott$/, (url, option) => [
				'',
				[
					'Kaamelott',
					'Kaamelott/Yvain',
				],
			])

			for (const [ urlPattern, fileContent ] of fakeResponse) {
				fetchMocked.get(urlPattern, fileContent)
			}
		})

		afterEach(fetchMocked.restore)
	}



	it('should return an empty Map if there are an empty search term', async () => {
		const emptyQuotePage = await getFullQuotePage()
		emptyQuotePage.should.be.a('Map').empty
	})


	it('should pass the happy path', async () => {
		const quotePageMap = await getFullQuotePage('Kaamelott', 'fr')
		quotePageMap.size.should.be.equal(isFrozenEnvironment ? 2 : 31)

		const mainUrl = 'https://fr.wikiquote.org/wiki/Kaamelott'
		quotePageMap.has(mainUrl).should.be.true
		if (isFrozenEnvironment) {
			quotePageMap.get(mainUrl).should.have.lengthOf(71032)
		}
		else {
			quotePageMap.get(mainUrl).should.have.lengthOf.at.least(71000)
		}

		const secondUrl = 'https://fr.wikiquote.org/wiki/Kaamelott%2FYvain'
		quotePageMap.has(secondUrl).should.be.true
		if (isFrozenEnvironment) {
			quotePageMap.get(secondUrl).should.have.lengthOf(14398)
		}
		else {
			quotePageMap.get(secondUrl).should.have.lengthOf.at.least(14000)
		}
	})


	it('should be fast enough to resolve all content', function() {
		const delay = isFrozenEnvironment ? 50 : 1000
		this.timeout(delay)

		return getFullQuotePage('Kaamelott', 'fr')
	})
})
