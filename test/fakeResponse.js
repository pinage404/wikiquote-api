import en_search_match_nothing from './response/search/en_match_nothing.json'
import fr_search_kaamelott from './response/search/fr_kaamelott.json'
import en_search_south_park from './response/search/en_south_park.json'

import fr_page_not_found from './response/getQuotePage/fr_page_not_found.json'
import fr_page_ambiguous from './response/getQuotePage/fr_page_ambiguous.json'
import en_page_ambiguous from './response/getQuotePage/en_page_ambiguous.json'
import fr_main_page from './response/getQuotePage/fr_main_page.json'
import fr_secondary_page from './response/getQuotePage/fr_secondary_page.json'
import fr_main_alternative_page from './response/getQuotePage/fr_main_alternative_page.json'


const fakeResponse = new Map([
	[ /en\.wikiquote\.org.+&search=this_is_a_search_to_a_unknown_page$/,
		en_search_match_nothing, ],
	[ /fr\.wikiquote\.org.+&search=Kaamelott$/,
		fr_search_kaamelott, ],
	[ /en\.wikiquote\.org.+&search=south%20PARK$/,
		en_search_south_park, ],

	[ /fr\.wikiquote\.org.+&page=This_page_should_never_exist_or_this_test_will_fail$/,
		fr_page_not_found, ],
	[ /fr\.wikiquote\.org.+&page=V%20pour%20Vendetta$/,
		fr_page_ambiguous, ],
	[ /en\.wikiquote\.org.+&page=Harry%20Potter$/,
		en_page_ambiguous, ],
	[ /fr\.wikiquote\.org.+&page=Kaamelott$/,
		fr_main_page, ],
	[ /fr\.wikiquote\.org.+&page=Kaamelott%2FYvain$/,
		fr_secondary_page, ],
	[ /fr\.wikiquote\.org.+&page=Hero%20Corp$/,
		fr_main_alternative_page, ],
])


export default fakeResponse
