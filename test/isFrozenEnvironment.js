const isFrozenEnvironment = process.env.NODE_ENV ? process.env.NODE_ENV !== 'production' : true

export default isFrozenEnvironment
