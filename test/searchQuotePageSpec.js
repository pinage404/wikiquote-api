import chai from 'chai'
chai.should()

import fetchMocked from 'fetch-mock'


import isFrozenEnvironment from './isFrozenEnvironment'
import fakeResponse from './fakeResponse'
import expected_search_fr_kaamelott from './expected/search/fr_kaamelott.json'
import expected_search_en_south_park from './expected/search/en_south_park.json'


import searchQuotePage from '../src/searchQuotePage'

import { UnsupportedLanguageException, NoSuchPageException, DisambiguationPageException } from '../src/exception'



describe('Wikiquote API search quotes pages', function() {
	this.timeout(isFrozenEnvironment ? 300 : 2000)

	if (isFrozenEnvironment) {
		beforeEach(() => {
			for (const [ urlPattern, fileContent ] of fakeResponse) {
				fetchMocked.get(urlPattern, fileContent)
			}
		})

		afterEach(fetchMocked.restore)
	}



	it('should return an empty list if there are an empty search term', async () => {
		const emptyPageList = await searchQuotePage()
		emptyPageList.should.be.a('array').empty
	})


	it('should raise exception on unsupported language', () => {
		const unsupportedLang = searchQuotePage('Kaamelott', 'zh')
		return unsupportedLang.should.be.rejectedWith(UnsupportedLanguageException, 'Unsupported language: “zh”')
	})


	it('should return empty list if there are no page found', async () => {
		const emptyPageList = await searchQuotePage('this_is_a_search_to_a_unknown_page')
		emptyPageList.should.be.a('array').empty
	})


	it('should return all pages related to a topic', async () => {
		const pageList = await searchQuotePage('Kaamelott', 'fr')
		pageList.should.have.lengthOf(31)

		if (isFrozenEnvironment) {
			pageList.should.be.deep.equal(expected_search_fr_kaamelott)
		}
	})


	it('should be case insensitive', async () => {
		const pageList = await searchQuotePage('south PARK', 'en')
		pageList.should.have.lengthOf(19)

		if (isFrozenEnvironment) {
			pageList.should.be.deep.equal(expected_search_en_south_park)
		}
	})
})
