import '../polyfill/ElementMatches'


import chai from 'chai'
chai.should()

import fetchMocked from 'fetch-mock'


import isFrozenEnvironment from './isFrozenEnvironment'
import fakeResponse from './fakeResponse'


import getQuotePageDOM from '../src/getQuotePageDOM'



describe('Wikiquote API get quote page DOM', function() {
	this.timeout(isFrozenEnvironment ? 500 : 5000)

	if (isFrozenEnvironment) {
		beforeEach(() => {
			for (const [ urlPattern, fileContent ] of fakeResponse) {
				fetchMocked.get(urlPattern, fileContent)
			}
		})

		afterEach(fetchMocked.restore)
	}



	it('should return an empty <div></div> if there are an empty search term', async () => {
		const emptyDiv = await getQuotePageDOM()
		emptyDiv.should.an.instanceof(HTMLElement)
		emptyDiv.matches('div:empty').should.be.true
	})


	it('should return the DOM of the quotes page', async () => {
		const html = await getQuotePageDOM('Hero Corp', 'fr')
		html.matches('div:not(:empty)').should.be.true

		if (isFrozenEnvironment) {
			html.querySelectorAll('*').should.have.lengthOf(997)
		}
		else {
			html.querySelectorAll('*').should.have.lengthOf.at.least(900)
		}
	})
})
