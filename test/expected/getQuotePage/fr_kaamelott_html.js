const expected_fr_kaamelott_html = `<div class="mw-parser-output"><p><i><b><a href="/wiki/Cat%C3%A9gorie:Kaamelott" title="Catégorie:Kaamelott">Kaamelott</a></b></i> est une <a href="/wiki/Cat%C3%A9gorie:S%C3%A9rie_t%C3%A9l%C3%A9vis%C3%A9e" title="Catégorie:Série télévisée">série télévisée</a> humoristique et dramatique française dont la diffusion originale a eu lieu entre 2005 et 2009 en <a href="/wiki/France" title="France">France</a>. (Une suite est prévue au cinéma à partir de 2017.)</p>
<p></p>
<h2><span class="mw-headline" id="Citations_des_personnages">Citations des personnages</span></h2>
<h3><span class="mw-headline" id="Angharad"><a href="https://fr.wikipedia.org/wiki/Personnages_de_Kaamelott#Angharad" class="extiw" title="w:Personnages de Kaamelott">Angharad</a></span></h3>
<p><span class="citation">Hé ben, si un jour j’oublie que je suis boniche, vous serez gentil de me le rappeler&#160;!</span></p>
<ul>
<li>
<div class="ref">Vanessa Guedj, <i>Kaamelott</i>, Livre I, 37&#160;: <i>La Romance de Lancelot</i>, écrit par Alexandre Astier.</div>
</li>
</ul>
<p><br /></p>
<p><span class="citation">Si Monsieur et Madame préfèrent s'envoyer des fions dans l'intimité, je peux aussi me retirer.</span></p>
<ul>
<li>
<div class="ref">Vanessa Guedj, <i>Kaamelott</i>, Livre II, 37&#160;: <i>La Joute Ancillaire</i>, écrit par Alexandre Astier.</div>
</li>
</ul>
<p><br /></p>
<p><span class="citation">(<i>À Arthur et à Mevanwi ensemble dans leur lit</i>) Entre le calme plat du temps de Madame et la guérilla de cette nuit, je me permets de dire à Monsieur que Monsieur n'a pas bien le sens de la mesure. Monsieur est en dents de scie...</span></p>
<ul>
<li>
<div class="ref">Vanessa Guedj, <i>Kaamelott</i>, Livre IV, 26&#160;: <i>La chambre de la Reine</i>, écrit par Alexandre Astier.</div>
</li>
</ul>
<p><br /></p>
<p><span class="citation">(<i>À Arthur au sujet de Mevanwi</i>) Je dois avouer que la chose a du bon, si c'est pour m'éviter la vue de la grande décarade, le défilé des nouvelles têtes, la parade des amoureuses à calcul, les poids lourds de la jambe légère, le festival de la morue, en somme...</span></p>
<ul>
<li>
<div class="ref">Vanessa Guedj, <i>Kaamelott</i>, Livre IV, 30&#160;: <i>Les Bonnes</i>, écrit par Alexandre Astier.</div>
</li>
</ul>
<p><br /></p>
<h3><span class="mw-headline" id="Anna"><a href="https://fr.wikipedia.org/wiki/Personnages_de_Kaamelott#Anna" class="extiw" title="w:Personnages de Kaamelott">Anna</a></span></h3>
<p><span class="citation">Quand on veut être sûr de son coup, mon petit bonhomme, on plante des carottes, on ne joue pas les chefs d'État&#160;!</span></p>
<ul>
<li>
<div class="ref">Anouk Grinberg, <i>Kaamelott</i>, Livre V, 30&#160;: <i>La Supplique</i>, écrit par Alexandre Astier.</div>
</li>
</ul>
<p><br /></p>
<p><span class="citation">(<i>S'adressant au roi Loth</i>) Excusez-moi, est-ce qu'à un seul moment, j'aurais par mégarde donné le moindre signe de vouloir discuter avec vous?</span></p>
<ul>
<li>
<div class="ref">Anouk Grinberg, <i>Kaamelott</i>, Livre V, 30&#160;: <i>La Supplique</i>, écrit par Alexandre Astier.</div>
</li>
</ul>
<p><br /></p>
<p><span class="citation">Vous êtes une gigantesque tarlouze.</span></p>
<ul>
<li>
<div class="ref">Anouk Grinberg, <i>Kaamelott</i>, Livre V, 30&#160;: <i>La Supplique</i>, écrit par Alexandre Astier.</div>
</li>
</ul>
<p><br /></p>
<p><span class="citation">Jusqu'à présent, vous cumuliez trois statuts&#160;: bâtard, fils d'assassin et usurpateur. Bâtard, vous le serez toujours, de même que vous serez toujours le fils de votre sale ordure de père. Mais aujourd'hui, vous n'êtes plus roi, vous n'usurpez plus votre titre, c'est déjà ça.</span></p>
<ul>
<li>
<div class="ref">Anouk Grinberg, <i>Kaamelott</i>, Livre V, 30&#160;: <i>La Supplique</i>, écrit par Alexandre Astier.</div>
</li>
</ul>
<p><br /></p>
<h3><span class="mw-headline" id="Appius_Manilius"><a href="https://fr.wikipedia.org/wiki/Personnages_de_Kaamelott#Appius_Manilius" class="extiw" title="w:Personnages de Kaamelott">Appius Manilius</a></span></h3>
<p><span class="citation">(<i>au Maître d'Armes</i>) J'pensais à une chose, en toute amitié, un gros pain dans votre tête, ça serait de nature à vous convenir&#160;?</span></p>
<ul>
<li>
<div class="ref">Emmanuel Meirieu, <i>Kaamelott</i>, Livre VI, 6&#160;: <i>Nuptiae</i>, écrit par Alexandre Astier.</div>
</li>
</ul>
<p><br /></p>
<p><span class="citation">En fait, je m'en suis pris deux, mais j'ai cherché la merde.</span></p>
<ul>
<li>
<div class="ref">Emmanuel Meirieu, <i>Kaamelott</i>, Livre VI, 6&#160;: <i>Nuptiae</i>, écrit par Alexandre Astier.</div>
</li>
</ul>
<p><br /></p>
<p><span class="citation">Ouais c'est bien qu'on reste un peu dehors, comme ça j'pourrais vous mettre une grosse tarte en plein air.</span></p>
<ul>
<li>
<div class="ref">Emmanuel Meirieu, <i>Kaamelott</i>, Livre VI, 7&#160;: <i>Arturus Rex</i>, écrit par Alexandre Astier.</div>
</li>
</ul>
<p><br /></p>
<p><span class="citation">Désolé mais vous l'avez chié votre mariage. Ce s'rait rien si c'était pas juste la deuxième fois.</span></p>
<ul>
<li>
<div class="ref">Emmanuel Meirieu, <i>Kaamelott</i>, Livre VI, 8&#160;: <i>Lacrimosa</i>, écrit par Alexandre Astier.</div>
</li>
</ul>
<p><br /></p>
<h3><span class="mw-headline" id="Arthur"><a href="https://fr.wikipedia.org/wiki/Personnages_de_Kaamelott#Arthur" class="extiw" title="w:Personnages de Kaamelott">Arthur</a></span></h3>
<dl>
<dd><i>Voir le recueil de citations&#160;:</i> <a href="/wiki/Kaamelott/Arthur" title="Kaamelott/Arthur">Arthur</a></dd>
</dl>
<h3><span class="mw-headline" id="Attila"><a href="https://fr.wikipedia.org/wiki/Personnages_de_Kaamelott#Attila" class="extiw" title="w:Personnages de Kaamelott">Attila</a></span></h3>
<p><span class="citation">Je vais tout casser, ici, MOI! Kaamelott Kaamelott&#160;: y va rester un tas de caaaailloux, comme ça! Je veux l'or, tout l'or sinon c'est la guerre&#160;!</span></p>
<ul>
<li>
<div class="ref">Lan Truong, <i>Kaamelott</i>, Livre I, 89&#160;: <i>Le Fléau de Dieu</i>, écrit par Alexandre Astier.</div>
</li>
</ul>
<p><br /></p>
<p><span class="citation">Je te mettrai à genoux, Arthur de Bretagne&#160;!</span></p>
<ul>
<li>
<div class="ref">Lan Truong, <i>Kaamelott</i>, Livre I, 89&#160;: <i>Le Fléau de Dieu</i>, écrit par Alexandre Astier.</div>
</li>
</ul>
<p><br /></p>
<p><span class="citation">Pourquoi pas&#160;?</span></p>
<ul>
<li>
<div class="ref">Lan Truong, <i>Kaamelott</i>, Livre I, 89&#160;: <i>Le Fléau de Dieu</i>, écrit par Alexandre Astier.</div>
</li>
</ul>
<p><br /></p>
<p><span class="citation">Alors les femmes. [...] Donnez nous les femmes&#160;! [...] Tous les femmes&#160;!!! [...] AAAAAAHH&#160;!!!</span></p>
<ul>
<li>
<div class="ref">Lan Truong, <i>Kaamelott</i>, Livre I, 89&#160;: <i>Le Fléau de Dieu</i>, écrit par Alexandre Astier.</div>
</li>
</ul>
<p><br /></p>
<p><span class="citation">Arthur&#160;! Kaamelott, c’est zéro&#160;! Des cailloux, des cailloux, des cailloux, ça m'énerve&#160;!&#160;!</span></p>
<ul>
<li>
<div class="ref">Lan Truong, <i>Kaamelott</i>, Livre III, 12&#160;: <i>Le Fléau de Dieu II</i>, écrit par Alexandre Astier.</div>
</li>
</ul>
<p><br /></p>
<p><span class="citation">Cette fois-ci, on part avec les femmes&#160;! HAHAAAHA&#160;!!!!</span></p>
<ul>
<li>
<div class="ref">Lan Truong, <i>Kaamelott</i>, Livre III, 12&#160;: <i>Le Fléau de Dieu II</i>, écrit par Alexandre Astier.</div>
</li>
</ul>
<p><br /></p>
<h3><span class="mw-headline" id="Belt"><a href="https://fr.wikipedia.org/wiki/Personnages_de_Kaamelott#Belt" class="extiw" title="w:Personnages de Kaamelott">Belt</a></span></h3>
<p><span class="citation"><i>(Beurré)</i> Sire, vous êtes quand même un sacré souverain. Accueillir des péquenots qui sentent la bouse, comme ça, dans votre chapeau, ben je dis château&#160;!</span></p>
<ul>
<li>
<div class="ref">François Morel, <i>Kaamelott</i>, Livre II, 68&#160;: <i>Spiritueux</i>, écrit par Alexandre Astier.</div>
</li>
</ul>
<p><br /></p>
<h3><span class="mw-headline" id="P.C3.A8re_Blaise"><a href="https://fr.wikipedia.org/wiki/Personnages_de_Kaamelott#P.C3.A8re_Blaise" class="extiw" title="w:Personnages de Kaamelott">Père Blaise</a></span></h3>
<dl>
<dd><i>Voir le recueil de citations&#160;:</i> <a href="/wiki/Kaamelott/P%C3%A8re_Blaise" title="Kaamelott/Père Blaise">Père Blaise</a></dd>
</dl>
<h3><span class="mw-headline" id="Bohort"><a href="https://fr.wikipedia.org/wiki/Personnages_de_Kaamelott#Bohort" class="extiw" title="w:Personnages de Kaamelott">Bohort</a></span></h3>
<dl>
<dd><i>Voir le recueil de citations&#160;:</i> <a href="/wiki/Kaamelott/Bohort" title="Kaamelott/Bohort">Bohort</a></dd>
</dl>
<h3><span class="mw-headline" id="Breccan"><a href="https://fr.wikipedia.org/wiki/Personnages_de_Kaamelott#Breccan" class="extiw" title="w:Personnages de Kaamelott">Breccan</a></span></h3>
<p><span class="citation">Sire, on en a déjà parlé de la pierre&#160;! Je peux pas monter une pierre d’une toise et demi dans un escalier à colimaçon&#160;!</span></p>
<ul>
<li>
<div class="ref">Yvan le Bolloc'h, <i>Kaamelott</i>, Livre I, épisode 3&#160;: <i>La table de Breccan</i>, écrit par Alexandre Astier.</div>
</li>
</ul>
<p><br /></p>
<p><span class="citation">Le cuir, ça restera toujours le cuir. Le cuir, ça traverse les âges, les frontières, les modes. D’autant qu’là j’vous ai pas mis d’la vache moisie. Attention&#160;! C’est d’la tannerie d’luxe&#160;! Assemblée au crochet de six&#160;! Y'a des heures de main d'œuvre derrière&#160;!</span></p>
<ul>
<li>
<div class="ref">Yvan le Bolloc'h, <i>Kaamelott</i>, Livre I, épisode 3 <i>La table de Breccan</i>, écrit par Alexandre Astier.</div>
</li>
</ul>
<p><br /></p>
<p><span class="citation">Ça vous dérange si… si j’mets ma p’tite griffe là&#160;? Si y a un chef de clan qui vient poser ses miches par là devant et il trouve ça cossu. Ben le jour où il voudra faire un buffet ou un plumard sur mesure... il saura où m’trouver, quoi.</span></p>
<ul>
<li>
<div class="ref">Yvan le Bolloc'h, <i>Kaamelott</i>, Livre I, épisode 3 <i>La table de Breccan</i>, écrit par Alexandre Astier.</div>
</li>
</ul>
<p><br /></p>
<p><span class="citation">Ben, d'la réclame beau Dieu&#160;!</span></p>
<ul>
<li>
<div class="ref">Yvan le Bolloc'h, <i>Kaamelott</i>, Livre I, épisode 3 <i>La table de Breccan</i>, écrit par Alexandre Astier.</div>
</li>
</ul>
<p><br /></p>
<p><span class="citation">C’est carré-carré chez vous, hein&#160;?</span></p>
<ul>
<li>
<div class="ref">Yvan le Bolloc'h, <i>Kaamelott</i>, Livre I, épisode 3 <i>La table de Breccan</i>, écrit par Alexandre Astier.</div>
</li>
</ul>
<p><br /></p>
<p><span class="citation">Après, moi, pour le détail, je sais pas</span></p>
<ul>
<li>
<div class="ref">Yvan le Bolloc'h, <i>Kaamelott</i>, Livre I, épisode 3 <i>La table de Breccan</i>, écrit par Alexandre Astier.</div>
</li>
</ul>
<p><br /></p>
<h3><span class="mw-headline" id="Le_roi_burgonde"><a href="https://fr.wikipedia.org/wiki/Personnages_de_Kaamelott#Le_roi_burgonde" class="extiw" title="w:Personnages de Kaamelott">Le roi burgonde</a></span></h3>
<dl>
<dd><i>Voir le recueil de citations&#160;:</i> <a href="/wiki/Kaamelott/Le_roi_burgonde" title="Kaamelott/Le roi burgonde">Le roi burgonde</a></dd>
</dl>
<h3><span class="mw-headline" id="Caius_Camillus"><a href="https://fr.wikipedia.org/wiki/Personnages_de_Kaamelott#Caius_Camillus" class="extiw" title="w:Personnages de Kaamelott">Caius Camillus</a></span></h3>
<dl>
<dd><i>Voir le recueil de citations&#160;:</i> <a href="/wiki/Kaamelott/Caius_Camillus" title="Kaamelott/Caius Camillus">Caius Camillus</a></dd>
</dl>
<h3><span class="mw-headline" id="Calogrenant"><a href="https://fr.wikipedia.org/wiki/Personnages_de_Kaamelott#Calogrenant" class="extiw" title="w:Personnages de Kaamelott">Calogrenant</a></span></h3>
<p><span class="citation">Passe de côté, esquive, attaque droite, hop&#160;! La dague dans la main gauche, garde inversée et crac&#160;! Dix pouces de ferraille dans les côtelettes&#160;!</span></p>
<ul>
<li>
<div class="ref">Stéphane Margot, <i>Kaamelott</i>, décembre 2004, promotion de la série, écrit par Alexandre Astier.</div>
</li>
</ul>
<p><br /></p>
<p><span class="citation">Vous n'avez qu'à considérer que je suis officiellement cul nu.</span></p>
<ul>
<li>
<div class="ref">Stéphane Margot, <i>Kaamelott</i>, Livre I, 59&#160;: <i>La jupe de Calogrenant</i>, écrit par Alexandre Astier.</div>
</li>
</ul>
<p><br /></p>
<p><span class="citation">Quoi&#160;? Vous voulez que j'humilie ma terre natale pour une connerie d'armure rouillée&#160;?</span></p>
<ul>
<li>
<div class="ref">Stéphane Margot, <i>Kaamelott</i>, Livre I, 59&#160;: <i>La jupe de Calogrenant</i>, écrit par Alexandre Astier.</div>
</li>
</ul>
<p><br /></p>
<p><span class="citation">(<i>En parlant à Arthur d'un cadeau offert par un clan saxon</i>) Mais c'est un petit mâle, il s'appelle ... "Ferme ta gueule", c'est que quand il est né il arrêtait pas de couiner...</span></p>
<ul>
<li>
<div class="ref">Stéphane Margot, <i>Kaamelott</i>, Livre II, 46&#160;: <i>Le Cadeau</i>, écrit par Alexandre Astier.</div>
</li>
</ul>
<p><br /></p>
<p><span class="citation">Attendez, on dit chef de clan parce qu'il faut dire quelque chose, hein&#160;! Mais Conran, il doit y avoir trois cahutes sur la plage, ils sont une dizaine à tout casser, et c'est tous des clodos, alors...</span></p>
<ul>
<li>
<div class="ref">Stéphane Margot, <i>Kaamelott</i>, Livre II, 60&#160;: <i>L'Alliance</i>, écrit par Alexandre Astier.</div>
</li>
</ul>
<p><br /></p>
<p><span class="citation"><i>(En Parlant d'Hervé de Rinel)</i> Ah mais c'est ça qui pue!</span></p>
<ul>
<li>
<div class="ref">Stéphane Margot, <i>Kaamelott</i>, Livre II, 60&#160;: <i>L'Alliance</i>, écrit par Alexandre Astier.</div>
</li>
</ul>
<p><br /></p>
<h3><span class="mw-headline" id="Capito"><a href="https://fr.wikipedia.org/wiki/Personnages_de_Kaamelott#Publius_Servius_Capito" class="extiw" title="w:Personnages de Kaamelott">Capito</a></span></h3>
<p><span class="citation">(Un sénateur&#160;: - J'vais vous dire&#160;: le jour où j'ai accepté d'être sénateur, j'aurais mieux fait de m'péter une jambe.) Ça peut encore s'arranger.</span></p>
<ul>
<li>
<div class="ref">François Levantal, <i>Kaamelott</i>, Livre VI, 1&#160;: <i>Miles Ignotus</i>, écrit par Alexandre Astier.</div>
</li>
</ul>
<p><br /></p>
<p><span class="citation">Ça s'enferme les registres&#160;! (Glaucia&#160;: Ah bon&#160;? ) Ouais&#160;! Et les trous du cul qui font pas leur boulot, ça s'enferme aussi&#160;!</span></p>
<ul>
<li>
<div class="ref">François Levantal, <i>Kaamelott</i>, Livre VI, 1&#160;: <i>Miles Ignotus</i>, écrit par Alexandre Astier.</div>
</li>
</ul>
<p><br /></p>
<p><span class="citation">(Glaucia&#160;: - Ave Publius Servius Capito, t'avais demandé un rendez-vous&#160;? ) Non. J'ai pas envoyé de fleurs non plus.</span></p>
<ul>
<li>
<div class="ref">François Levantal, <i>Kaamelott</i>, Livre VI, 1&#160;: <i>Miles Ignotus</i>, écrit par Alexandre Astier.</div>
</li>
</ul>
<p><br /></p>
<p><span class="citation"><i>(S'adressant à Merlin)</i> Vous préférez monter dans la barque, maintenant, de plein gré, ou dans vingt secondes avec un coup de pompe dans l'oignon&#160;?</span></p>
<ul>
<li>
<div class="ref">François Levantal, <i>Kaamelott</i>, Livre VI, 2&#160;: <i>Centurio</i>, écrit par Alexandre Astier.</div>
</li>
</ul>
<p><br /></p>
<p><span class="citation">Tu salues plus les généraux&#160;? C'est pourtant largement au-dessus de ton grade merdique.</span></p>
<ul>
<li>
<div class="ref">François Levantal, <i>Kaamelott</i>, Livre VI, 5&#160;: <i>Dux bellorum</i>, écrit par Alexandre Astier.</div>
</li>
</ul>
<p><br /></p>
<p><span class="citation">Alors, sans vouloir te commander, aurais-tu l'obligeance de bien vouloir te magner le cul.</span></p>
<ul>
<li>
<div class="ref">François Levantal, <i>Kaamelott</i>, Livre VI, 5&#160;: <i>Dux bellorum</i>, écrit par Alexandre Astier.</div>
</li>
</ul>
<p><br /></p>
<p><span class="citation">Sans rire, vous foutez quoi là-dedans là&#160;? Pourtant, vous croulez pas sous les visites diplomatiques, il me semble&#160;? Une fois tous les dix ans, vous pouvez donner un coup de balai, non&#160;?</span></p>
<ul>
<li>
<div class="ref">François Levantal, <i>Kaamelott</i>, Livre VI, 8&#160;: <i>Lacrimosa</i>, écrit par Alexandre Astier.</div>
</li>
</ul>
<p><br /></p>
<p><span class="citation">J'ai à peu près compris le principe du double jeu, mais je ne saurais dire pourquoi, j'ai l'impression que tu nous l'as mis dans l'os&#160;? Et je pense que si tu refous les pieds à Rome, ne le prends pas mal hein... je te ferai éliminer.</span></p>
<ul>
<li>
<div class="ref">François Levantal, <i>Kaamelott</i>, Livre VI, 8&#160;: <i>Lacrimosa</i>, écrit par Alexandre Astier.</div>
</li>
</ul>
<p><br /></p>
<h3><span class="mw-headline" id="C.C3.A9sar"><a href="https://fr.wikipedia.org/wiki/Personnages_de_Kaamelott#C.C3.A9sar" class="extiw" title="w:Personnages de Kaamelott">César</a></span></h3>
<p><span class="citation">A l'époque, quand je levais le doigt, y avait 15 000 soldats qui gueulaient Imperator&#160;! Maintenant, quand je lève le doigt, c'est pour aller pisser...</span></p>
<ul>
<li>
<div class="ref">Pierre Mondy, <i>Kaamelott</i>, Livre VI, 4&#160;: <i>Arturi Inquisitio</i>, écrit par Alexandre Astier.</div>
</li>
</ul>
<p><br /></p>
<p><span class="citation">Imperator&#160;! Aaaaah&#160;! Imperator&#160;! Ah ça fait du bien crénom&#160;! Ca me change de tous ces glands là... Votre tranquillité par-ci, votre tranquillité par-là... Ça, dis donc... Pour être tranquille, 24h sur 24 au plumard... À pioncer, à bouffer de la compote... Ça, c'est sûr que j'enchaîne pas les crises de nerf... Mais assieds-toi là&#160;! On dirait que tu vas chanter une chanson&#160;! Allez&#160;!</span></p>
<ul>
<li>
<div class="ref">Pierre Mondy, <i>Kaamelott</i>, Livre VI, 4&#160;: <i>Arturi Inquisitio</i>, écrit par Alexandre Astier.</div>
</li>
</ul>
<p><br /></p>
<p><span class="citation">On devient pas chef parce qu'on le mérite andouille&#160;! On devient chef par un concours de circonstances, on le mérite après&#160;! Moi, il m'a p'têt fallu dix ans pour mériter mon grade, si pas vingt. Tous les jours, j'ai travaillé pour pas nager dans mon uniforme. Y a pas trente-six solutions. Arturus&#160;? Hein&#160;? Fais semblant&#160;! Fais semblant d'être Dux. Fais semblant de mériter ton grade. Fais semblant d'être un grand chef de guerre. Si tu fais bien semblant, un jour tu verras, t'auras plus besoin&#160;!</span></p>
<ul>
<li>
<div class="ref">Pierre Mondy, <i>Kaamelott</i>, Livre VI, 5&#160;: <i>Dux bellorum</i>, écrit par Alexandre Astier.</div>
</li>
</ul>
<p><br /></p>
<p><span class="citation">Mais moi non plus, je suis pas le chef suprême de la première puissance mondiale&#160;! Le chef suprême de la première puissance mondiale, c'est celui qui tire les ficelles dans l'arrière-boutique, moi je suis juste un spectacle de marionnettes. Voilà. La petite journée désarticulée de César le pantin. Et ça se passe dans le ghetto&#160;!</span></p>
<ul>
<li>
<div class="ref">Pierre Mondy, <i>Kaamelott</i>, Livre VI, 6&#160;: <i>Nuptiæ</i>, écrit par Alexandre Astier.</div>
</li>
</ul>
<p><br /></p>
<p><span class="citation">[S'adressant à Arthur.] Des chefs de guerre, il y en a de toutes sortes. Des bons, des mauvais, des pleines cagettes, il y en a. Mais une fois de temps en temps, il en sort un. Exceptionnel. Un héros. Une légende. Des chefs comme ça, il y en a presque jamais. Mais tu sais ce qu'ils ont tous en commun&#160;? Tu sais ce que c'est, leur pouvoir secret&#160;? Ils ne se battent que pour la dignité des faibles.</span></p>
<ul>
<li>
<div class="ref">Pierre Mondy, <i>Kaamelott</i>, Livre VI, 6&#160;: <i>Nuptiæ</i>, écrit par Alexandre Astier.</div>
</li>
</ul>
<p><br /></p>
<p><span class="citation">[César offre à Arthur la bague de contrôle des lames] C'est pour t'apprendre à faire confiance à la magie, parce qu'il n'y a que ça qui marche sur Terre, Arturus, la magie. Le reste, ça ne vaut pas un rond.</span></p>
<ul>
<li>
<div class="ref">Pierre Mondy, <i>Kaamelott</i>, Livre VI, 6&#160;: <i>Nuptiæ</i>, écrit par Alexandre Astier.</div>
</li>
</ul>
<p><br /></p>
<h3><span class="mw-headline" id="Cryda_de_Tintagel"><a href="https://fr.wikipedia.org/wiki/Personnages_de_Kaamelott#Cryda_de_Tintagel" class="extiw" title="w:Personnages de Kaamelott">Cryda de Tintagel</a></span></h3>
<p><span class="citation">Allez, vous devriez mettre les bouts, les demi-sels&#160;! C'est gentil d'être passés&#160;! On va vous faire un p'tit sac avec des restes pour manger chez vous.</span></p>
<ul>
<li>
<div class="ref">Claire Nadeau, <i>Kaamelott</i>, Livre VI, 8&#160;: <i>Lacrimosa</i>, écrit par Alexandre Astier.</div>
</li>
</ul>
<p><br /></p>
<h3><span class="mw-headline" id="Dagonet"><a href="https://fr.wikipedia.org/wiki/Personnages_de_Kaamelott#Dagonet" class="extiw" title="w:Personnages de Kaamelott">Dagonet</a></span></h3>
<p><span class="citation">Le premier truc qui frappe, c’est la langue. Vous savez ce que c’est, on arrive sur place, boum… En fait, c’est quand on commence à entendre les gens parler que là… là, on se dit&#160;: je suis pas chez moi.</span></p>
<ul>
<li>
<div class="ref">Antoine de Caunes, <i>Kaamelott</i>, Livre I, 27&#160;: <i>De retour de Judée</i>, écrit par Alexandre Astier.</div>
</li>
</ul>
<p><br /></p>
<p><span class="citation">[Le vieux] a essayé de me vendre un genre de turban, comme ils se mettent sur la tête, là-bas. J'ai d'abord commencé par l'envoyer chier, puis je me suis dit que ça ferait sûrement plaisir au seigneur Karadoc.</span></p>
<ul>
<li>
<div class="ref">Antoine de Caunes, <i>Kaamelott</i>, Livre I, 27&#160;: <i>Le Retour de Judée</i>, écrit par Alexandre Astier.</div>
</li>
</ul>
<p><br /></p>
<p><span class="citation">Heureusement qu’on n'a aucune dignité… Sinon on serait bien dans la merde.</span></p>
<ul>
<li><span class="precisions">S'adressant au roi Loth et au seigneur Galessin</span></li>
</ul>
<ul>
<li>
<div class="ref">Antoine de Caunes, <i>Kaamelott</i>, Livre IV, 99/100&#160;: <i>Le Désordre et la Nuit</i>, écrit par Alexandre Astier.</div>
</li>
</ul>
<p><br /></p>
<p><span class="citation">Eh... Il a pas inventé le plat de la main morte, celui-là.</span></p>
<ul>
<li>
<div class="ref">Antoine de Caunes, <i>Kaamelott</i>, Livre V, 22&#160;: <i>La promesse</i>, écrit par Alexandre Astier.</div>
</li>
</ul>
<p><br /></p>
<h3><span class="mw-headline" id="La_Dame_du_Lac"><a href="https://fr.wikipedia.org/wiki/Personnages_de_Kaamelott#La_Dame_du_Lac" class="extiw" title="w:Personnages de Kaamelott">La Dame du Lac</a></span></h3>
<p><span class="citation">Il faut dormir un peu, maintenant. Si si, maintenant. Parce que vous êtes fatigué. Et quand vous vous réveillerez, vous aurez oublié vos soucis.</span></p>
<ul>
<li>
<div class="ref">Audrey Fleurot, <i>Kaamelott</i>, janvier 2005, promotion de la série, écrit par Alexandre Astier.</div>
</li>
</ul>
<p><br /></p>
<p><span class="citation">J'ai dit "elle aussi"&#160;? […] Oh, ça m'étonne, ça... Vous êtes sûr que j'ai pas dit "elle est sympa"&#160;?</span></p>
<ul>
<li>
<div class="ref">Audrey Fleurot, <i>Kaamelott</i>, Livre III, 98&#160;: <i>Hollow Man</i>, écrit par Alexandre Astier.</div>
</li>
</ul>
<p><br /></p>
<p><span class="citation">Vous n’avez aucun courage. Aucune dignité. Votre manque de foi est en train de tout détruire.</span></p>
<ul>
<li>
<div class="ref">Audrey Fleurot, <i>Kaamelott</i>, Livre IV, 2&#160;: <i>Tous Les Matins du Monde</i>, écrit par Alexandre Astier.</div>
</li>
</ul>
<p><br /></p>
<p><span class="citation">(<i>Terrorisée</i>) Ils m'ont ba... Ils m'ont ba... (Arthur essaie divers mots) ILS M'ONT BANNIE!</span></p>
<ul>
<li>
<div class="ref">Audrey Fleurot, <i>Kaamelott</i>, Livre IV, 28&#160;: <i>La Révoquée</i>, écrit par Alexandre Astier.</div>
</li>
</ul>
<p><br /></p>
<p><span class="citation">Au départ, je devais vous guider vers la lumière, vers le Graal. Mais vous, vous préférez n'en faire qu'à votre tête&#160;: Lancelot s'en va&#160;? Pas grave, qu'il s'en aille&#160;! Guenièvre part avec lui&#160;? Eh ben qu'elle se tire, ça fera de l'air&#160;! Il faut jamais convoiter la femme d'un autre chevalier&#160;? Ben eh, je m'en fous, moi. Je la convoite quand même&#160;! Résultat&#160;: Bannie&#160;! Ah vous pouvez être fier de vous&#160;! (elle pleure)</span></p>
<ul>
<li>
<div class="ref">Audrey Fleurot, <i>Kaamelott</i>, Livre IV, 28&#160;: <i>La Révoquée</i>, écrit par Alexandre Astier.</div>
</li>
</ul>
<p><br /></p>
<p><span class="citation">Je sais ni boire, ni manger, ni m’habiller, ni me laver, ni rien&#160;! Vous sentez que je vais être un fardeau pour vous, ou pas&#160;?</span></p>
<ul>
<li><span class="precisions">S'adressant à Arthur.</span></li>
</ul>
<ul>
<li>
<div class="ref">Audrey Fleurot, <i>Kaamelott</i>, Livre IV, 28&#160;: <i>La Révoquée</i>, écrit par Alexandre Astier.</div>
</li>
</ul>
<p><br /></p>
<p><span class="citation">Gardez votre épée et servez-vous-en pour trouver le Graal&#160;! [Arthur replante Excalibur]</span></p>
<ul>
<li>
<div class="ref">Audrey Fleurot, <i>Kaamelott</i>, Livre V, 12&#160;: <i>La roche et le fer</i> , écrit par Alexandre Astier.</div>
</li>
</ul>
<p><br /></p>
<p><span class="citation">Hé&#160;! Hééé&#160;! J'suis pas une spécialiste, mais, à mon avis, là, vous allez mourir, hein&#160;!</span></p>
<ul>
<li>
<div class="ref">Audrey Fleurot, <i>Kaamelott</i>, Livre V, 36&#160;: <i>La Nourrice</i>, écrit par Alexandre Astier.</div>
</li>
</ul>
<p><br /></p>
<h3><span class="mw-headline" id="Demetra"><a href="https://fr.wikipedia.org/wiki/Personnages_de_Kaamelott#Demetra" class="extiw" title="w:Personnages de Kaamelott">Demetra</a></span></h3>
<dl>
<dd><i>Voir le recueil de citations&#160;:</i> <a href="/wiki/Kaamelott/Demetra" title="Kaamelott/Demetra">Demetra</a></dd>
</dl>
<h3><span class="mw-headline" id="Drusilla"><a href="https://fr.wikipedia.org/wiki/Personnages_de_Kaamelott#Drusilla" class="extiw" title="w:Personnages de Kaamelott">Drusilla</a></span></h3>
<p><span class="citation">Mille excuses... Normalement, je devrais pas dire ça à un prêtre chrétien, mais, comme vous m'avez tout l'air d'être un gros baltringue euh, je me permets&#160;? (<i>Père Blaise&#160;: Mmmh&#160;?...</i>) Vous avez oublié les alliances.</span></p>
<ul>
<li>
<div class="ref">Anne Benoît, <i>Kaamelott</i>, Livre VI, 6&#160;: <i>Nuptiæ</i>, écrit par Alexandre Astier.</div>
</li>
</ul>
<p><br /></p>
<p><span class="citation">Je préviens monsieur et madame que s'ils ont dans l'idée de remplacer leur hypothétique progéniture par des groupes d'amis dans le style de celui-ci, en ce qui me concerne, y a d'la démission dans l'air.</span></p>
<ul>
<li>
<div class="ref">Anne Benoît, <i>Kaamelott</i>, Livre VI, 6&#160;: <i>Nuptiæ</i>, écrit par Alexandre Astier.</div>
</li>
</ul>
<p><br /></p>
<h3><span class="mw-headline" id="Le_duc_d.E2.80.99Aquitaine"><a href="https://fr.wikipedia.org/wiki/Personnages_de_Kaamelott#Le_duc_d.E2.80.99Aquitaine" class="extiw" title="w:Personnages de Kaamelott">Le duc d’Aquitaine</a></span></h3>
<p><span class="citation">(<i>Présentant sa nouvelle femme à Seli, Arthur et Leodagan</i>)<br />
Ah mais non... Mais vous vous êtes encore sur l'ancienne&#160;! […]<br />
Tout à fait, excusez-moi, j'ai pas percuté. Non non, l'autre elle est morte. Heu... Les articulations soutenaient plus son poids. Donc elle a commencé par se remplir d'eau par les coudes et après c'est passé aux chevilles, c'est remonté aux genoux et un matin, elle avait tellement gonflé que j'ai appelé la garde.<br />
Alors heu... Ils sont venus, ils lui ont mis un coup de lance et puis elle a essayé de se... de se faufiler comme ça, pour se cacher sous le buffet.<br />
Pis elle passait pas parce que bon ben... Elle avait plus conscience de sa masse donc elle est restée là, comme ça, pis... Bah six heures après, elle était crevée.<br />
Donc, la Duchesse d'Aquitaine&#160;!</span></p>
<ul>
<li>
<div class="ref">Alain Chabat, <i>Kaamelott</i>, Livre V, 16&#160;: <i>Les Fruits d'Hiver</i>, écrit par Alexandre Astier.</div>
</li>
</ul>
<p><br /></p>
<p><span class="citation">(<i>Récitant d’un ton peu convaincant</i>) «&#160;Si c’est ma tête qui vous revient pas, vous pouvez toujours aller roupiller dans le couloir. […] Et à partir de maintenant, si j’entends un mot plus haut que l’autre je vous… renvoie dans votre bled d’at… natal à coups de pied dans… dans le fion. Comme ça vous pourrez aller ratisser la bouse et torcher le cul des poules, ça vous fera prendre l’air.&#160;»</span></p>
<ul>
<li>
<div class="ref">Alain Chabat, <i>Kaamelott</i>, Livre V, 19&#160;: <i>Les Nocturnales</i>, écrit par Alexandre Astier.</div>
</li>
</ul>
<p><br /></p>
<p><span class="citation">Non, mais on peut adapter. […] On peut dire, euh… <small>bon, je dis&#160;:</small> […] «&#160;Avec votre dégaine… euh… […] Avec votre dégaine de crevette, faites gaffe à pas vous faire bouffer par un mérou.&#160;»</span></p>
<ul>
<li>
<div class="ref">Alain Chabat, <i>Kaamelott</i>, Livre V, 23&#160;: <i>Le Forfait</i>, écrit par Alexandre Astier.</div>
</li>
</ul>
<p><br /></p>
<h3><span class="mw-headline" id="Edern"><a href="https://fr.wikipedia.org/wiki/Personnages_de_Kaamelott#Edern" class="extiw" title="w:Personnages de Kaamelott">Edern</a></span></h3>
<p><span class="citation">Mais c’est pas juste, ça&#160;! J’ai jamais porté de rose, hein. J’aime pas les fleurs, je me lave pas et je pisse dans mon armure, comme vous&#160;!</span></p>
<ul>
<li>
<div class="ref">Émilie Dequenne, <i>Kaamelott</i>, épisodes pilotes, 6&#160;: <i>Le Chevalier femme</i>, écrit par Alexandre Astier.</div>
</li>
</ul>
<p><br /></p>
<p><span class="citation">Quoi, c'est parce que je préfère les hommes c'est ça&#160;? À ce compte-là, faut virer Bohort aussi...</span></p>
<ul>
<li>
<div class="ref">Émilie Dequenne, <i>Kaamelott</i>, épisodes pilotes, 6&#160;: <i>Le Chevalier femme</i>, écrit par Alexandre Astier.</div>
</li>
</ul>
<p><br /></p>
<p><span class="citation">Tapette, mais c'est qui qui se trimballe en robe à travers tout le pays? […] Moi j'te parle p'tite fofolle!!</span></p>
<ul>
<li>
<div class="ref">Émilie Dequenne, <i>Kaamelott</i>, épisodes pilotes, 6&#160;: <i>Le Chevalier femme</i>, écrit par Alexandre Astier.</div>
</li>
</ul>
<p><br /></p>
<p><span class="citation">Dehors le cureton! Dehors le cureton!</span></p>
<ul>
<li>
<div class="ref">Émilie Dequenne, <i>Kaamelott</i>, épisodes pilotes, 6&#160;: <i>Le Chevalier femme</i>, écrit par Alexandre Astier.</div>
</li>
</ul>
<p><br /></p>
<h3><span class="mw-headline" id=".C3.89lias_de_Kelliwic.E2.80.99h"><a href="https://fr.wikipedia.org/wiki/Personnages_de_Kaamelott#.C3.89lias_de_Kelliwic.E2.80.99h" class="extiw" title="w:Personnages de Kaamelott">Élias de Kelliwic’h</a></span></h3>
<dl>
<dd><i>Voir le recueil de citations&#160;:</i> <a href="/wiki/Kaamelott/%C3%89lias_de_Kelliwic%E2%80%99h" title="Kaamelott/Élias de Kelliwic’h">Élias de Kelliwic’h</a></dd>
</dl>
<h3><span class="mw-headline" id="Galessin"><a href="https://fr.wikipedia.org/wiki/Personnages_de_Kaamelott#Galessin" class="extiw" title="w:Personnages de Kaamelott">Galessin</a></span></h3>
<p><span class="citation">Ouais&#160;! Qu'il y vienne patauger dans la merde&#160;!</span></p>
<ul>
<li>
<div class="ref">Alexis Hénon, <i>Kaamelott</i>, épisodes pilotes, 6&#160;: <i>Le Chevalier Femme</i>, écrit par Alexandre Astier.</div>
</li>
</ul>
<p><br /></p>
<p><span class="citation">Nous on est Celtes&#160;! Et on est fiers de l'être&#160;!</span></p>
<ul>
<li>
<div class="ref">Alexis Hénon, <i>Kaamelott</i>, épisodes pilotes, 6&#160;: <i>Le Chevalier Femme</i>, écrit par Alexandre Astier.</div>
</li>
</ul>
<p><br /></p>
<p><span class="citation">Vous avez qu’à dire au pape que s’il trouve que ça traîne, il n’a qu’à venir le chercher lui-même, son gobelet à la con&#160;!</span></p>
<ul>
<li>
<div class="ref">Alexis Hénon, <i>Kaamelott</i>, décembre 2004, promotion de la série, écrit par Alexandre Astier.</div>
</li>
</ul>
<p><br /></p>
<p><span class="citation"><i>(Au Père Blaise)</i> Il nous parle bien de travers, le cureton, aujourd'hui&#160;!</span></p>
<ul>
<li>
<div class="ref">Alexis Hénon, <i>Kaamelott</i>, Livre I, 51&#160;: <i>Enluminures</i>, écrit par Alexandre Astier.</div>
</li>
</ul>
<p><br /></p>
<p><span class="citation">Moi, à une époque, je voulais faire vœu de pauvreté (...) Mais avec le pognon que j'rentrais, j'arrivais pas à concilier les deux.</span></p>
<ul>
<li>
<div class="ref">Alexis Hénon, <i>Kaamelott</i>, Livre II, 45&#160;: <i>Les Vœux</i>, écrit par Alexandre Astier.</div>
</li>
</ul>
<p><br /></p>
<p><span class="citation"><i>(A Lancelot)</i> Han le lèche-pompe! On vous dérange Apollon? C'est votre casque qui vous gêne&#160;?</span></p>
<ul>
<li>
<div class="ref">Alexis Hénon, <i>Kaamelott</i>, Livre II, 84&#160;: <i>Spagenhelm</i>, écrit par Alexandre Astier.</div>
</li>
</ul>
<p><br />
<i>(A Dagonnet)</i></p>
<p><span class="citation">Oui... Ben non&#160;!</span></p>
<ul>
<li>
<div class="ref">Alexis Hénon, <i>Kaamelott</i>, Livre IV, 14&#160;: <i>Dagonnet et le cadastre</i>, écrit par Alexandre Astier.</div>
</li>
</ul>
<p><br /></p>
<p><span class="citation"><i>(A Lancelot)</i> Et ben allez-y, recevez-le dans la merde. Y'a pas un siège pour s'asseoir, pas un truc à lui offrir à boire (...) C'est le roi d'Orcanie quand même. C'est pas Jo le Clodo.</span></p>
<ul>
<li>
<div class="ref">Alexis Hénon, <i>Kaamelott</i>, Livre IV, 67&#160;: <i>Loth et le Graal</i>, écrit par Alexandre Astier.</div>
</li>
</ul>
<p><br /></p>
<p><span class="citation">Vous me faites penser à ces types qui se servent jamais en premier pour faire poli, et quand c'est à eux...ils prennent tout le fromage gratiné et les autres, ils sont obligés de manger le légume seul.</span></p>
<ul>
<li>
<div class="ref">Alexis Hénon, <i>Kaamelott</i>, Livre V, 21&#160;: <i>Aux yeux de tous III</i>, écrit par Alexandre Astier.</div>
</li>
</ul>
<p><br /></p>
<h3><span class="mw-headline" id="Gauvain"><a href="https://fr.wikipedia.org/wiki/Personnages_de_Kaamelott#Gauvain" class="extiw" title="w:Personnages de Kaamelott">Gauvain</a></span></h3>
<dl>
<dd><i>Voir le recueil de citations&#160;:</i> <a href="/wiki/Kaamelott/Gauvain" title="Kaamelott/Gauvain">Gauvain</a></dd>
</dl>
<h3><span class="mw-headline" id="Goustan"><a href="https://fr.wikipedia.org/wiki/Personnages_de_Kaamelott#Goustan" class="extiw" title="w:Personnages de Kaamelott">Goustan</a></span></h3>
<p><span class="citation"><i>(Aux rois assemblés)</i> FUMIEEEERS&#160;!!!!</span></p>
<ul>
<li>
<div class="ref">Philippe Nahon, <i>Kaamelott</i>, Livre VI, 7&#160;: <i>Arturus Rex</i>, écrit par Alexandre Astier.</div>
</li>
</ul>
<p><br /></p>
<p><span class="citation"><i>( À Arthur au sujet de l'héritier )</i> Je comprends qu'avec deux livres de viande sur l'estomac, on ne soit pas très actif dans une chambre à coucher. Ça ronfle à faire craquer le joint des murs.</span></p>
<ul>
<li>
<div class="ref">Philippe Nahon, <i>Kaamelott</i>, Livre I, 63&#160;: <i>Goustan Le Cruel</i>, écrit par Alexandre Astier.</div>
</li>
</ul>
<p><br /></p>
<p><span class="citation">C'est justement parce que vous avez été cocollé par une lopette de jardinier que vous gouvernez comme une femme&#160;!</span></p>
<ul>
<li>
<div class="ref">Philippe Nahon, <i>Kaamelott</i>, Livre I, 63&#160;: <i>Goustan le Cruel</i>, écrit par Alexandre Astier.</div>
</li>
</ul>
<p><br /></p>
<p><span class="citation">Allons, je suis humilié. Moi qui croyais que le Romain respectait ses ennemis, et ben non&#160;! En voilà encore une belle désillusion. S'ils croient que c'est avec une mentalité pareille qu'ils vont fédérer le pays, on leur rend leur fric, on remonte sur les chevaux, et on leur met sur la gueule&#160;! D'un autre côté, restituer une pareille quantité de blé, ça frise le mauvais gout, je suis désolé, ça ne se fait pas&#160;! Ne serait-ce par respect pour les plus nécessiteux. Bon allons, après, il y a le serment&#160;: si je garde le blé, j'attaque pas&#160;! Et ben voilà&#160;: je garde le blé, j'attaque pas; un serment c'est sacré. Oui mais ce qui m'ennuie, c'est que c'est pile le jour que j'avais choisi pour vous passer le pouvoir, c'est quelque chose, les dates, hein&#160;! Je garde le blé, j'attaque pas, je suis sous serment. Mais vous, maintenant que vous êtes roi de Carmélide, si... s'il vous prend le goût d'aller leur dérouiller le cul, je ne vois vraiment pas comment je pourrais vous en empêcher&#160;!</span></p>
<ul>
<li>
<div class="ref">Philippe Nahon, <i>Kaamelott</i>, Livre VI, 1: <i>Miles Ignotus</i>, écrit par Alexandre Astier.</div>
</li>
</ul>
<p><br /></p>
<p><span class="citation">Alors c'est ça, la stratégie moderne? Réunir cinq trous-de-balle en cercle et s'balancer des fions?</span></p>
<ul>
<li>
<div class="ref">Philippe Nahon, <i>Kaamelott</i>, Livre VI, 2&#160;: <i>Centurio</i>, écrit par Alexandre Astier.</div>
</li>
</ul>
<p><br /></p>
<p><span class="citation">Alors écoutez-moi bien les sent-la-pisse, vous êtes probablement en train d'insulter un roi désigné par les dieux, et s'il y a une chose dont il ne faut pas se foutre, c'est les dieux&#160;!</span></p>
<ul>
<li>
<div class="ref">Philippe Nahon, <i>Kaamelott</i>, Livre VI, 7&#160;: <i>Arturus Rex</i>, écrit par Alexandre Astier.</div>
</li>
</ul>
<p><br /></p>
<p><span class="citation"><i>( À Arthur et Guenièvre)</i> Hé&#160;! A l'origine j'étais sorti pour lâcher une caisse, mais quand on vous voit comme ça dans le clair de lune, on a pas envie de bousiller le tableau. J'vais aller loufer à l'intérieur pour emboucaner les autres fumiers. Profitez&#160;! Le plus beau dans les histoires d'amour, c'est le début...</span></p>
<ul>
<li>
<div class="ref">Philippe Nahon, <i>Kaamelott</i>, Livre VI, 7&#160;: <i>Arturus rex</i>, écrit par Alexandre Astier.</div>
</li>
</ul>
<p><br /></p>
<p><span class="citation">Je vous ai passé le pouvoir, je vais pas vous le reprendre. Mais faites attention de pas devenir une tarlouse.</span></p>
<ul>
<li>
<div class="ref">Philippe Nahon, <i>Kaamelott</i>, Livre VI, 8&#160;: <i>Lacrimosa</i>, écrit par Alexandre Astier.</div>
</li>
</ul>
<p><br /></p>
<p><span class="citation">Et ben vous inviterez vos copains à la maison, on vous organisera une petite après-midi à la plage et vous pourrez faire des pâtés&#160;!.</span></p>
<ul>
<li>
<div class="ref">Philippe Nahon, <i>Kaamelott</i>, Livre VI, 2&#160;: <i>Centurio</i>, écrit par Alexandre Astier.</div>
</li>
</ul>
<p><br /></p>
<p><span class="citation">Allez&#160;! Et si on part assez tôt, on pourra peut-être s'arrêter deux minutes si on tombe sur des champis&#160;!.</span></p>
<ul>
<li>
<div class="ref">Philippe Nahon, <i>Kaamelott</i>, Livre VI, 2&#160;: <i>Centurio</i>, écrit par Alexandre Astier.</div>
</li>
</ul>
<p><br /></p>
<h3><span class="mw-headline" id="Gr.C3.BCd.C3.BC"><a href="https://fr.wikipedia.org/wiki/Personnages_de_Kaamelott#Gr.C3.BCd.C3.BC" class="extiw" title="w:Personnages de Kaamelott">Grüdü</a></span></h3>
<p><span class="citation">J'ai rêvé qu'il y avait des scorpions qui voulaient me piquer. En plus, y en avait un il était mi-ours, mi-scorpion et re mi-ours derrière&#160;!</span></p>
<ul>
<li>
<div class="ref">Thibault Roux, <i>Kaamelott</i>, Livre I, 22&#160;: <i>La queue du scorpion</i>, écrit par Alexandre Astier.</div>
</li>
</ul>
<p><br /></p>
<p><span class="citation">Non, sans blague, Seigneur Bohort, avec tout le respect que je vous dois, la prochaine fois que je vous revois tourner autour de la chambre du roi, je vous déglingue la tête.</span></p>
<ul>
<li>
<div class="ref">Thibault Roux, <i>Kaamelott</i>, Livre I, 29&#160;: <i>L’Assassin de Kaamelott</i>, écrit par Alexandre Astier.</div>
</li>
</ul>
<p><br /></p>
<p><span class="citation">Moi, un jour j’ai rêvé qu’y avait un type qui venait me voir. Il me dit&#160;: «&#160;Vous avez jamais connu vos parents&#160;?&#160;» J’lui réponds «&#160;non&#160;». Et là, il me dit&#160;: «&#160;Eh ben, vous inquiétez pas, ils sont fromagers.&#160;»</span></p>
<ul>
<li>
<div class="ref">Thibault Roux, <i>Kaamelott</i>, Livre IV, 64&#160;: <i>Le Rêve d’Ygerne</i>, écrit par Alexandre Astier.</div>
</li>
</ul>
<p><br /></p>
<h3><span class="mw-headline" id="Gueni.C3.A8vre"><a href="https://fr.wikipedia.org/wiki/Personnages_de_Kaamelott#Gueni.C3.A8vre" class="extiw" title="w:Personnages de Kaamelott">Guenièvre</a></span></h3>
<dl>
<dd><i>Voir le recueil de citations&#160;:</i> <a href="/wiki/Kaamelott/Gueni%C3%A8vre" title="Kaamelott/Guenièvre">Guenièvre</a></dd>
</dl>
<h3><span class="mw-headline" id="Guethenoc"><a href="https://fr.wikipedia.org/wiki/Personnages_de_Kaamelott#Guethenoc" class="extiw" title="w:Personnages de Kaamelott">Guethenoc</a></span></h3>
<dl>
<dd><i>Voir le recueil de citations&#160;:</i> <a href="/wiki/Kaamelott/Guethenoc" title="Kaamelott/Guethenoc">Guethenoc</a></dd>
</dl>
<h3><span class="mw-headline" id="Herv.C3.A9_de_Rinel"><a href="https://fr.wikipedia.org/wiki/Personnages_de_Kaamelott#Herv.C3.A9_de_Rinel" class="extiw" title="w:Personnages de Kaamelott">Hervé de Rinel</a></span></h3>
<p><span class="citation">Ben ils m'ont dit d'aller me faire mettre. Après, ils m'ont jeté des gadins et une marmite avec un restant de soupe de poisson.</span></p>
<ul>
<li>
<div class="ref">Tony Saba, <i>Kaamelott</i>, Livre II, 72&#160;: <i>L'Alliance</i>, écrit par Alexandre Astier.</div>
</li>
</ul>
<p><br /></p>
<p><span class="citation">J’ai pénétré leur lieu d'habitation de façon subrogative […] en tapinant.</span></p>
<ul>
<li>
<div class="ref">Tony Saba, <i>Kaamelott</i>, Livre III, 91&#160;: <i>L’Espion</i>, écrit par Alexandre Astier.</div>
</li>
</ul>
<p><br /></p>
<p><span class="citation">Ahhh, c'est pour ça que vous avez changé de siège, c'est parce que vous êtes pépé... Heu non, c'est parce que vous êtes roi.</span></p>
<ul>
<li>
<div class="ref">Tony Saba, <i>Kaamelott</i>, Livre V, 40&#160;: <i>Les Pionniers</i>, écrit par Alexandre Astier.</div>
</li>
</ul>
<p><br /></p>
<p><span class="citation">Y'a plusieurs personnes qui sont passées par votre siège là, et beeen, le tout premier, je crois qu'il s'appelait... Carbure, ça fait vachement longtemps qu'on l'a pas vu. Moi, je serais vous, je lancerais des recherches.</span></p>
<ul>
<li>
<div class="ref">Tony Saba, <i>Kaamelott</i>, Livre V, 42&#160;: <i>Le Destitué</i>, écrit par Alexandre Astier.</div>
</li>
</ul>
<p><br /></p>
<p><span class="citation">L'adjectif qui me correspond le mieux, c'est&#160;: le plancton.</span></p>
<ul>
<li>
<div class="ref">Tony Saba, <i>Kaamelott</i>, Livre VI, 7&#160;: <i>Arturus Rex</i>, écrit par Alexandre Astier.</div>
</li>
</ul>
<p><br /></p>
<h3><span class="mw-headline" id="L.27interpr.C3.A8te_burgonde"><a href="https://fr.wikipedia.org/wiki/Personnages_de_Kaamelott#L.27interpr.C3.A8te_burgonde" class="extiw" title="w:Personnages de Kaamelott">L'interprète burgonde</a></span></h3>
<p><span class="citation">Déjà, à la base, un Burgonde, c'est con comme une meule, alors celui-là&#160;! Ah, vous pouvez pas savoir c'que c'est que de tomber interprète avec un engin pareil&#160;! (<i>bruit de pet</i>) Tiens, vous voyez&#160;? Toute la journée, c'est comme ça&#160;! Il pue, il pète, il lâche des ruines&#160;! Tiens, l'autre jour à table... Il devient tout bleu. Il était en train de s'étouffer avec un os de caille, cet abruti&#160;! Il tousse, il crache, il re-tousse, et BINGO&#160;! Il m'dégueule dessus&#160;! Vous l'croyez, ça&#160;?</span></p>
<ul>
<li>
<div class="ref">Loránt Deutsch, <i>Kaamelott</i>, Livre I, 45&#160;: <i>L'interprète</i>, écrit par Alexandre Astier.</div>
</li>
</ul>
<p><br /></p>
<p><span class="citation">La culture burgonde&#160;? Je savais même pas qu’y en avait une… Non, moi je voulais faire grec moderne, mais y avait plus de place. Il restait que burgonde ou anglais. Aaaaanglais&#160;! Mais c’est encore moins répandu.</span></p>
<ul>
<li>
<div class="ref">Loránt Deutsch, <i>Kaamelott</i>, Livre I, 45&#160;: <i>L’Interprète</i>, écrit par Alexandre Astier.</div>
</li>
</ul>
<p><br /></p>
<h3><span class="mw-headline" id="Le_Seigneur_Jacca"><a href="https://fr.wikipedia.org/wiki/Personnages_de_Kaamelott#Jacca" class="extiw" title="w:Personnages de Kaamelott">Le Seigneur Jacca</a></span></h3>
<dl>
<dd><i>Voir le recueil de citations&#160;:</i> <a href="/wiki/Kaamelott/Le_Seigneur_Jacca" title="Kaamelott/Le Seigneur Jacca">Le Seigneur Jacca</a></dd>
</dl>
<h3><span class="mw-headline" id="Les_Jumelles_du_p.C3.AAcheur"><a href="https://fr.wikipedia.org/wiki/Personnages_de_Kaamelott#Les_Jumelles_du_p.C3.AAcheur" class="extiw" title="w:Personnages de Kaamelott">Les Jumelles du pêcheur</a></span></h3>
<p><span class="citation">Nous, si vous réfléchissez bien, on a quatre bras. Avec les vôtres, ça fait six&#160;! posez tout ça sur le trône, imaginez un peu les possibilités…</span></p>
<ul>
<li>
<div class="ref">Alexandra Saadoun et Magali Saadoun, <i>Kaamelott</i>, Livre IV, 21&#160;: <i>La Parade</i>, écrit par Alexandre Astier.</div>
</li>
</ul>
<p><br /></p>
<h3><span class="mw-headline" id="Le_Jurisconsulte"><a href="https://fr.wikipedia.org/wiki/Personnages_de_Kaamelott#Le_Jurisconsulte" class="extiw" title="w:Personnages de Kaamelott">Le Jurisconsulte</a></span></h3>
<p><span class="citation">Peau de vache.</span></p>
<ul>
<li>
<div class="ref">Christian Clavier, <i>Kaamelott</i>, Livre V, 27&#160;: <i>Le Jurisconsulte</i>, écrit par Alexandre Astier.</div>
</li>
</ul>
<p><br /></p>
<p><span class="citation">(<i>Au Père Blaise</i>)<br />
Z'êtes un glandeur. Un gros... GLANDEUR&#160;!!!</span></p>
<ul>
<li>
<div class="ref">Christian Clavier, <i>Kaamelott</i>, Livre V, 27&#160;: <i>Le Jurisconsulte</i>, écrit par Alexandre Astier.</div>
</li>
</ul>
<p><br /></p>
<p><span class="citation">Ah non&#160;! AH NON&#160;! On ne se vautre pas dans la bouffe, c'est INTOLÉRABLE&#160;!!! Vous allez arrrrrrêter&#160;!!!</span></p>
<ul>
<li>
<div class="ref">Christian Clavier, <i>Kaamelott</i>, Livre V, 40&#160;: <i>Les Pionniers</i>, écrit par Alexandre Astier.</div>
</li>
</ul>
<p><br /></p>
<p><span class="citation">Ah mais vous êtes pas ma mère&#160;! Si j'ai envie de mettre des grosses morues dans mon plumard, JE FAIS CE QUE JE VEUX&#160;!!! Soyez gentille, lâchez-moi la chemise&#160;!</span></p>
<ul>
<li>
<div class="ref">Christian Clavier, <i>Kaamelott</i>, Livre V, 41&#160;: <i>La Conspiratrice</i>, écrit par Alexandre Astier.</div>
</li>
</ul>
<p><br /></p>
<p><span class="citation">BANDES DE TROUS DU CUL&#160;!!!</span></p>
<ul>
<li>
<div class="ref">Christian Clavier, <i>Kaamelott</i>, Livre V, 41&#160;: <i>La Conspiratrice</i>, écrit par Alexandre Astier.</div>
</li>
</ul>
<p><br /></p>
<p><span class="citation">Mais vous êtes une grosse morue!</span></p>
<ul>
<li>
<div class="ref">Christian Clavier, <i>Kaamelott</i>, Livre V, 27&#160;: <i>Le Jurisconsulte</i>, écrit par Alexandre Astier.</div>
</li>
</ul>
<p><br /></p>
<h3><span class="mw-headline" id="Kadoc"><a href="https://fr.wikipedia.org/wiki/Personnages_de_Kaamelott#Kadoc" class="extiw" title="w:Personnages de Kaamelott">Kadoc</a></span></h3>
<dl>
<dd><i>Voir le recueil de citations&#160;:</i> <a href="/wiki/Kaamelott/Kadoc" title="Kaamelott/Kadoc">Kadoc</a></dd>
</dl>
<h3><span class="mw-headline" id="Karadoc"><a href="https://fr.wikipedia.org/wiki/Personnages_de_Kaamelott#Karadoc" class="extiw" title="w:Personnages de Kaamelott">Karadoc</a></span></h3>
<dl>
<dd><i>Voir le recueil de citations&#160;:</i> <a href="/wiki/Kaamelott/Karadoc" title="Kaamelott/Karadoc">Karadoc</a></dd>
</dl>
<h3><span class="mw-headline" id="Lancelot"><a href="https://fr.wikipedia.org/wiki/Personnages_de_Kaamelott#Lancelot" class="extiw" title="w:Personnages de Kaamelott">Lancelot</a></span></h3>
<dl>
<dd><i>Voir le recueil de citations&#160;:</i> <a href="/wiki/Kaamelott/Lancelot" title="Kaamelott/Lancelot">Lancelot</a></dd>
</dl>
<h3><span class="mw-headline" id="L.C3.A9odagan"><a href="https://fr.wikipedia.org/wiki/Personnages_de_Kaamelott#L.C3.A9odagan" class="extiw" title="w:Personnages de Kaamelott">Léodagan</a></span></h3>
<dl>
<dd><i>Voir le recueil de citations&#160;:</i> <a href="/wiki/Kaamelott/L%C3%A9odagan" title="Kaamelott/Léodagan">Léodagan</a></dd>
</dl>
<h3><span class="mw-headline" id="Loth"><a href="https://fr.wikipedia.org/wiki/Personnages_de_Kaamelott#Loth" class="extiw" title="w:Personnages de Kaamelott">Loth</a></span></h3>
<dl>
<dd><i>Voir le recueil de citations&#160;:</i> <a href="/wiki/Kaamelott/Loth" title="Kaamelott/Loth">Loth</a></dd>
</dl>
<h3><span class="mw-headline" id="Le_ma.C3.AEtre_d.E2.80.99armes"><a href="https://fr.wikipedia.org/wiki/Personnages_de_Kaamelott#Le_ma.C3.AEtre_d.E2.80.99armes" class="extiw" title="w:Personnages de Kaamelott">Le maître d’armes</a></span></h3>
<dl>
<dd><i>Voir le recueil de citations&#160;:</i> <a href="/wiki/Kaamelott/Le_ma%C3%AEtre_d%E2%80%99armes" title="Kaamelott/Le maître d’armes">Le maître d’armes</a></dd>
</dl>
<h3><span class="mw-headline" id="M.C3.A9l.C3.A9agant"><a href="https://fr.wikipedia.org/wiki/Personnages_de_Kaamelott#M.C3.A9l.C3.A9agant" class="extiw" title="w:Personnages de Kaamelott">Méléagant</a></span></h3>
<dl>
<dd><i>Voir le recueil de citations&#160;:</i> <a href="/wiki/Kaamelott/M%C3%A9l%C3%A9agant" title="Kaamelott/Méléagant">Méléagant</a></dd>
</dl>
<h3><span class="mw-headline" id="Manius_Macrinus_Firmus"><a href="https://fr.wikipedia.org/wiki/Personnages_de_Kaamelott#Manius_Macrinus_Firmus" class="extiw" title="w:Personnages de Kaamelott">Manius Macrinus Firmus</a></span></h3>
<p><span class="citation">Une nuit, une nuit à attendre... Une longue nuit avant de savoir si le souverain ennemi acceptera un traité de paix, si fragile, si friable qu'on aurait même de la peine à le prendre au sérieux. Des solutions précaires, du rafistolage, voilà tout ce que j'ai su inventer. La Bretagne résistait quand je suis arrivé, elle résistera encore quand je partirai. Je ne saurais dire pourquoi, je conserve encore, rescapée de mon découragement, une curiosité&#160;: existe t-il quelqu'un parmi nous déjà - ou encore à naitre - qui se destine à restaurer l'ordre sur l'île de Bretagne? Et s'il existe, que peut-il bien posséder que je ne possède moi-même&#160;? D'où vient-il? Est-il romain? Quelle arme tient-il à sa ceinture&#160;? Celui qui vaincra là où j'ai échoué, je voudrais voir son visage une fois, car je lui conserve encore, rescapé de mon découragement, ma curiosité.</span></p>
<ul>
<li>
<div class="ref">Tcheky Karyo, <i>Kaamelott</i>, Livre VI, 1&#160;: <i>Miles Ignotus</i>, écrit par Alexandre Astier.</div>
</li>
</ul>
<p><br /></p>
<p><span class="citation">C'est tout&#160;? Treize ans qu'ils me laissent pourrir sur pied, dans ce pays de merde&#160;! Treize ans&#160;! Maintenant ils m'envoient un message, il faut que je rentre chez moi&#160;! [...] Mais qu'est ce qui leur fait croire que ça existe encore chez moi&#160;?!! Est-ce qu'un type qui a foutu le camp treize ans peut encore avoir un chez soi&#160;? C'est complètement absurde...</span></p>
<ul>
<li>
<div class="ref">Tcheky Karyo, <i>Kaamelott</i>, Livre VI, 6&#160;: <i>Nuptiæ</i>, écrit par Alexandre Astier.</div>
</li>
</ul>
<p><br /></p>
<h3><span class="mw-headline" id="Merlin"><a href="https://fr.wikipedia.org/wiki/Personnages_de_Kaamelott#Merlin" class="extiw" title="w:Personnages de Kaamelott">Merlin</a></span></h3>
<dl>
<dd><i>Voir le recueil de citations&#160;:</i> <a href="/wiki/Kaamelott/Merlin" title="Kaamelott/Merlin">Merlin</a></dd>
</dl>
<h3><span class="mw-headline" id="Mevanwi"><a href="https://fr.wikipedia.org/wiki/Personnages_de_Kaamelott#Mevanwi" class="extiw" title="w:Personnages de Kaamelott">Mevanwi</a></span></h3>
<p><span class="citation">Je voudrais, pour une fois dans ma vie, ne pas avoir l’impression de dormir dans un chenil&#160;! […] Karadoc, soit vous montez dans ce bain, soit vous me perdez.</span></p>
<ul>
<li>
<div class="ref">Caroline Ferrus, <i>Kaamelott</i>, Livre II, 20&#160;: <i>Immaculé Karadoc</i>, écrit par Alexandre Astier.</div>
</li>
</ul>
<p><br /></p>
<p><span class="citation">Selon Karadoc, un lit n’est pas un lit s'il n’y a pas de quoi manger une semaine dedans sans en sortir.</span></p>
<ul>
<li>
<div class="ref">Caroline Ferrus, <i>Kaamelott</i>, Livre IV, 26&#160;: <i>La Chambre de la reine</i>, écrit par Alexandre Astier.</div>
</li>
</ul>
<p><br /></p>
<p><span class="citation">Une fois j'ai dormi avec un porc pendant une semaine. [Arthur: Un porc entier?] Un porc vivant.</span></p>
<ul>
<li>
<div class="ref">Caroline Ferrus, <i>Kaamelott</i>, Livre IV, 26&#160;: <i>La Chambre de la reine</i>, écrit par Alexandre Astier.</div>
</li>
</ul>
<p><br /></p>
<h3><span class="mw-headline" id="Perceval"><a href="https://fr.wikipedia.org/wiki/Personnages_de_Kaamelott#Perceval" class="extiw" title="w:Personnages de Kaamelott">Perceval</a></span></h3>
<dl>
<dd><i>Voir le recueil de citations&#160;:</i> <a href="/wiki/Kaamelott/Perceval" title="Kaamelott/Perceval">Perceval</a></dd>
</dl>
<h3><span class="mw-headline" id="Roparzh"><a href="https://fr.wikipedia.org/wiki/Personnages_de_Kaamelott#Roparzh" class="extiw" title="w:Personnages de Kaamelott">Roparzh</a></span></h3>
<dl>
<dd><i>Voir le recueil de citations&#160;:</i> <a href="/wiki/Kaamelott/Roparzh" title="Kaamelott/Roparzh">Roparzh</a></dd>
</dl>
<h3><span class="mw-headline" id="Lucius_Sillius_Sallustius"><a href="https://fr.wikipedia.org/wiki/Personnages_de_Kaamelott#Lucius_Sillius_Sallustius" class="extiw" title="w:Personnages de Kaamelott">Lucius Sillius Sallustius</a></span></h3>
<p><span class="citation">Regarde, vois&#160;! Cette glorieuse cité cosmopolite, regarde. Carrefour des civilisations, hôtesse orgueilleuse de peuplades fascinées, regarde. On nous reproche assez qu’il n’existe plus de vrais Romains, hein&#160;? Hein&#160;? Allez trouve un Breton, trouve.</span></p>
<ul>
<li>
<div class="ref">Patrick Chesnais, <i>Kaamelott</i>, Livre VI, 1&#160;: <i>Miles Ingnotus</i>, écrit par Alexandre Astier.</div>
</li>
</ul>
<p><br /></p>
<p><span class="citation">Ca y est&#160;! C’est leur dernière saison aux débiles. Tu peux préparer les outils, on passe directement de la discussion à la désintégration.</span></p>
<ul>
<li>
<div class="ref">Patrick Chesnais, <i>Kaamelott</i>, Livre VI, 4&#160;: <i>Arturi inquisitio</i>, écrit par Alexandre Astier.</div>
</li>
</ul>
<p><br /></p>
<p><span class="citation">(<i>Capito&#160;: Les quatre en une seule fois, vous avez pas peur que ça soit un peu voyant non&#160;?</i>) Rien à foutre&#160;! Tu peux même les crucifier sur une estrade au milieu du forum si ça te chante, c’est moi qui paye l’orchestre.</span></p>
<ul>
<li>
<div class="ref">Patrick Chesnais, <i>Kaamelott</i>, Livre VI, 4&#160;: <i>Arturi inquisitio</i>, écrit par Alexandre Astier.</div>
</li>
</ul>
<p><br /></p>
<p><span class="citation">Je vais poser une question simple&#160;: et vous, vous le méritez&#160;? Ah non mais attendez, est-ce que vous le méritez, est-ce que vous méritez votre place&#160;?! Lurco&#160;! Tu devais aller en Germanie... c'est ta tante qui t'a fait entrer au sénat&#160;! Pisansius&#160;! Pas une seule année dans la légion, pas une seule&#160;! J'm...e demande même parfois si t'as déjà tenu une arme dans ta vie... Destisius&#160;! C'est ton père qui a donné la moitié de ses terres pour que quelqu'un puisse prendre ta place en Afrique... C'est vrai ou c'est pas vrai&#160;? Vous êtes tous des planqués, tous, tous, tous, tous , tous. [...] Vous allez me signer cette tablette, parce que le p'tit mérite largement son grade autant que vous vous méritez votre place au Sénat... Largement.</span></p>
<ul>
<li>
<div class="ref">Patrick Chesnais, <i>Kaamelott</i>, Livre VI, 5&#160;: <i>Dux Bellorum</i>, écrit par Alexandre Astier.</div>
</li>
</ul>
<p><br /></p>
<h3><span class="mw-headline" id="S.C3.A9friane_d.27Aquitaine"><a href="https://fr.wikipedia.org/wiki/Personnages_de_Kaamelott#S.C3.A9friane_d.27Aquitaine" class="extiw" title="w:Personnages de Kaamelott">Séfriane d'Aquitaine</a></span></h3>
<p><span class="citation">C'est vrai que, dans votre position, il y a plus de chance de subir un siège plutôt que d'en organiser un.</span></p>
<ul>
<li>
<div class="ref">Axelle Laffont, <i>Kaamelott</i>, Livre III, 16&#160;: <i>Séfriane d'Aquitaine</i> , écrit par Alexandre Astier.</div>
</li>
</ul>
<p><br /></p>
<p><span class="citation">Je suis assez grande pour voir que si vous achetez des troncs d'arbres à ce prix-là, vous êtes en train de vous faire enfler&#160;!</span></p>
<ul>
<li>
<div class="ref">Axelle Laffont, <i>Kaamelott</i>, Livre III, 16&#160;: <i>Séfriane d'Aquitaine</i> , écrit par Alexandre Astier.</div>
</li>
</ul>
<p><br /></p>
<p><span class="citation">Vous croyez quoi&#160;? Que ça s'trouve dans l'cul des poules, le fric&#160;?</span></p>
<ul>
<li>
<div class="ref">Axelle Laffont, <i>Kaamelott</i>, Livre III, 16&#160;: <i>Séfriane d'Aquitaine</i> , écrit par Alexandre Astier.</div>
</li>
</ul>
<p><br /></p>
<p><span class="citation">Quand je vais raconter à mon oncle combien vous raquez pour vos béliers pourris, il va bien se marrer&#160;!</span></p>
<ul>
<li>
<div class="ref">Axelle Laffont, <i>Kaamelott</i>, Livre III, 16&#160;: <i>Séfriane d'Aquitaine</i> , écrit par Alexandre Astier.</div>
</li>
</ul>
<p><br /></p>
<p><span class="citation">Quoi&#160;? Qu'est-ce qu'y a&#160;? On cherche la marave&#160;?</span></p>
<ul>
<li>
<div class="ref">Axelle Laffont, <i>Kaamelott</i>, Livre III, 16&#160;: <i>Séfriane d'Aquitaine</i> , écrit par Alexandre Astier.</div>
</li>
</ul>
<p><br /></p>
<h3><span class="mw-headline" id="S.C3.A9li"><a href="https://fr.wikipedia.org/wiki/Personnages_de_Kaamelott#S.C3.A9li" class="extiw" title="w:Personnages de Kaamelott">Séli</a></span></h3>
<dl>
<dd><i>Voir le recueil de citations&#160;:</i> <a href="/wiki/Kaamelott/S%C3%A9li" title="Kaamelott/Séli">Séli</a></dd>
</dl>
<h3><span class="mw-headline" id="Spurius_Cordius_Frontinius"><a href="https://fr.wikipedia.org/wiki/Personnages_de_Kaamelott#Spurius_Cordius_Frontinius" class="extiw" title="w:Personnages de Kaamelott">Spurius Cordius Frontinius</a></span></h3>
<p><span class="citation">Oh allez, mon pote&#160;! Fais un effort&#160;! Dans dix minutes, t'es pendu et on n'en parle plus.</span></p>
<ul>
<li>
<div class="ref">Pascal Demolon, <i>Kaamelott</i>, Livre VI, 5&#160;: <i>Dux Bellorum</i>, écrit par Alexandre Astier.</div>
</li>
</ul>
<p><br /></p>
<p><span class="citation">D'autant que les orchestres en Bretagne, franchement, autant se frotter les noyaux avec des orties!</span></p>
<ul>
<li>
<div class="ref">Pascal Demolon, <i>Kaamelott</i>, Livre VI, 8&#160;: <i>Lacrimosa</i> , écrit par Alexandre Astier.</div>
</li>
</ul>
<p><br /></p>
<p><span class="citation">Oui ... et tu veux les jeter où, tes épluchures&#160;? ... en Germanie&#160;? Balance-moi tes saloperies, et radine sur le champs. PARCE-QUE QUAND je dis TOUT l'MONDE ... ET BEN C'EST TOUT L'MONDE&#160;! ... Tu es gentil.</span></p>
<ul>
<li>
<div class="ref">Pascal Demolon, <i>Kaamelott</i>, Livre VI, 8&#160;: <i>Lacrimosa</i> , écrit par Alexandre Astier.</div>
</li>
</ul>
<p><br /></p>
<h3><span class="mw-headline" id="Le_tavernier"><a href="https://fr.wikipedia.org/wiki/Personnages_de_Kaamelott#Le_tavernier" class="extiw" title="w:Personnages de Kaamelott">Le tavernier</a></span></h3>
<dl>
<dd><i>Voir le recueil de citations&#160;:</i> <a href="/wiki/Kaamelott/Le_tavernier" title="Kaamelott/Le tavernier">Le tavernier</a></dd>
</dl>
<h3><span class="mw-headline" id="Urgan"><a href="https://fr.wikipedia.org/wiki/Personnages_de_Kaamelott#Urgan" class="extiw" title="w:Personnages de Kaamelott">Urgan</a></span></h3>
<dl>
<dd><i>Voir le recueil de citations&#160;:</i> <a href="/wiki/Kaamelott/Urgan" title="Kaamelott/Urgan">Urgan</a></dd>
</dl>
<h3><span class="mw-headline" id="V.C3.A9rinus"><a href="https://fr.wikipedia.org/wiki/Personnages_de_Kaamelott#V.C3.A9rinus" class="extiw" title="w:Personnages de Kaamelott">Vérinus</a></span></h3>
<p><span class="citation">Ah mais ouais... Ah mais bien sûr, les gars... évidemment... nan mais allez-y, c'est mon truc les bateaux moi, je connais à mort... Allez-y hein... J'balance un caillou dans la flotte, y'en à trente qui r'montent.</span></p>
<ul>
<li>
<div class="ref">Manu Payet, <i>Kaamelott</i>, Livre VI, 1&#160;: <i>Miles Ignotus</i>, écrit par Alexandre Astier.</div>
</li>
</ul>
<p><br /></p>
<p><span class="citation">Ah ouais non mais attends, c'est du joli boulot là, les p'tits sacripants... Oui, sacripants, oui. C'est un terme un peu craignos. D'ailleurs, ben voilà, même craignos, c'est craignos. Mais c'est parce que je suis choqué&#160;! Qu'est-ce que j'entends&#160;? Tu demandes en mariage une personne âgée&#160;? Et la p'tite Julia alors, dans tout ça&#160;? Ah il faut que j'm'en occupe tout seul, c'est ça&#160;? Ah très bien... Merci les p'tits fripons... Tiens, ça aussi, c'est un peu craignos, tu vois&#160;? Mais c'est parce que là ouuuh&#160;! Et bravo général, beau boulot&#160;! Ah les pots cassés, c'est Verinus qui répare les pots cassés tout simplement... Ok d'accord, très bien...</span></p>
<ul>
<li>
<div class="ref">Manu Payet, <i>Kaamelott</i>, Livre VI, 6&#160;: <i>Nuptiæ</i>, écrit par Alexandre Astier.</div>
</li>
</ul>
<p><br /></p>
<p><span class="citation">Y a un truc, quand même, qu'il faut que vous compreniez bien les gars... Si vous voulez, moi, à la base... J'suis une balance... C'est le postulat de départ, on a devant soi, une balance. Donc une personne, si vous voulez, qu'on n'a pas besoin de cogner puisque elle vient elle-même délivrer l'information, sans que vous ayez même à la demander. Parce que non, pourquoi j'vous dis ça&#160;? Puisque... on est quand même à une heure et demie de marrons dans la gueule là... Et si j'vous dis que je ne sais rien, c'est que je pense qu'effectivement... (<i>Procyon le frappe</i>) Pfff... je n'sais rien...</span></p>
<ul>
<li>
<div class="ref">Manu Payet, <i>Kaamelott</i>, Livre VI, 8&#160;: <i>Lacrimosa</i>, écrit par Alexandre Astier.</div>
</li>
</ul>
<p><br /></p>
<p><span class="citation">Ben dis donc, j'sais pas c'que c'est, "la pire", mes p'tits cousins, mais j'peux vous garantir que, vu d'ici, ça fout les boules, hein... Ouuuh... Non, parce que, quand il a dit euh... plusieurs options, j'ai fait toute une série de p'tits pets comme ça, pou, pou, pou...</span></p>
<ul>
<li>
<div class="ref">Manu Payet, <i>Kaamelott</i>, Livre VI, 8&#160;: <i>Lacrimosa</i>, écrit par Alexandre Astier.</div>
</li>
</ul>
<p><br /></p>
<h3><span class="mw-headline" id="Venec"><a href="https://fr.wikipedia.org/wiki/Personnages_de_Kaamelott#Venec" class="extiw" title="w:Personnages de Kaamelott">Venec</a></span></h3>
<dl>
<dd><i>Voir le recueil de citations&#160;:</i> <a href="/wiki/Kaamelott/Venec" title="Kaamelott/Venec">Venec</a></dd>
</dl>
<h3><span class="mw-headline" id="Ygerne"><a href="https://fr.wikipedia.org/wiki/Personnages_de_Kaamelott#Ygerne" class="extiw" title="w:Personnages de Kaamelott">Ygerne</a></span></h3>
<p><span class="citation">Du temps de Pendragon, on avait le sens du dramatique&#160;: le Lac, Stonehenge, Avalon… Maintenant, dès qu’ils croisent un dragon, ils font un meeting.</span></p>
<ul>
<li>
<div class="ref">Josée Drevon, <i>Kaamelott</i>, décembre 2004, promotion de la série, écrit par Alexandre Astier.</div>
</li>
</ul>
<p><br /></p>
<p><span class="citation">(<i>À propos du chateau de Kaamelott</i>) Vulgaire&#160;? Oui... Mais pas seulement.</span></p>
<ul>
<li>
<div class="ref">Josée Drevon, <i>Kaamelott</i>, Livre I, 59&#160;: <i>La Visite d'Ygerne</i>, écrit par Lionnel Astier.</div>
</li>
</ul>
<p><br /></p>
<p><span class="citation">[Arthur&#160;: Vous mangez à quelle heure, vous, à Tintagel?] A Tintagel, on mange quand on l'a mérité, quand on sait qu'on a accompli ses commandements avec humilité et qu'on a glorifié sa famille [Arthur&#160;: Oui, non, nous, on mange quand on a faim.]</span></p>
<ul>
<li>
<div class="ref">Josée Drevon, <i>Kaamelott</i>, Livre I, 59&#160;: <i>La Visite d'Ygerne</i>, écrit par Lionnel Astier.</div>
</li>
</ul>
<p><br /></p>
<p><span class="citation">(<i>En parlant d'Arthur et en le regardant d'un air narquois</i>) Au cas où il aurait une petite "faim-faim"!?</span></p>
<ul>
<li>
<div class="ref">Josée Drevon, <i>Kaamelott</i>, Livre I, 59&#160;: <i>La Visite d'Ygerne</i>, écrit par Lionnel Astier.</div>
</li>
</ul>
<p><br /></p>
<p><span class="citation">(<i>À Arthur</i>) Dîtes tout de suite que vous avez honte de votre éducation.</span></p>
<ul>
<li>
<div class="ref">Josée Drevon, <i>Kaamelott</i>, Livre II, 62&#160;: <i>Le secret d’Arthur</i>, écrit par Alexandre Astier.</div>
</li>
</ul>
<p><br /></p>
<p><span class="citation">Mais je vous empêche pas de faire la guerre, mais vous la ferez guéri&#160;!</span></p>
<ul>
<li>
<div class="ref">Josée Drevon, <i>Kaamelott</i>, Livre II, 69&#160;: <i>Mater Dixit</i>, écrit par Lionnel Astier.</div>
</li>
</ul>
<p><br /></p>
<p><span class="citation">Allez y! Défilez les petites fourmis. Mettez vous bien en rang. Venez mesurer votre risible destin à celui des Pendragon. Venez constater la navrante insignifiance de vos existences. Vous avez pensé que les dieux allaient enfin reconnaitre en vous le souverain que personne n'avait vu? Vous alliez enfin donner au monde le spectacle de votre avènement. Quel roi suis-je&#160;? Un roi sévère&#160;? un roi conquérant&#160;? Vous n'êtes rien. Rien&#160;! Vous retournerez chez vous comme vous en êtes parti… Anonyme&#160;! Paisiblement, mon fils viendra reprendre ce qui est à lui. Et vous mettrez un genou en terre. Et vous baisserez les yeux. Et vous jurerez fidélité. Car c'est tout ce qu'il vous reste&#160;!</span></p>
<ul>
<li>
<div class="ref">Josée Drevon, <i>Kaamelott</i>, Livre V, 24&#160;: <i>Le dernier jour</i>, écrit par Lionnel Astier.</div>
</li>
</ul>
<p><br /></p>
<p><span class="citation">Vous êtes des porcs&#160;!</span></p>
<ul>
<li>
<div class="ref">Josée Drevon, <i>Kaamelott</i>, Livre V, 69&#160;: <i>Anton</i>, écrit par Lionnel Astier.</div>
</li>
</ul>
<p><br /></p>
<h3><span class="mw-headline" id="Yvain"><a href="https://fr.wikipedia.org/wiki/Personnages_de_Kaamelott#Yvain" class="extiw" title="w:Personnages de Kaamelott">Yvain</a></span></h3>
<dl>
<dd><i>Voir le recueil de citations&#160;:</i> <a href="/wiki/Kaamelott/Yvain" title="Kaamelott/Yvain">Yvain</a></dd>
</dl>
<h2><span class="mw-headline" id="Extraits_de_dialogues">Extraits de dialogues</span></h2>
<dl>
<dd><i>Voir le recueil de citations&#160;:</i> <a href="/wiki/Kaamelott/Dialogues" title="Kaamelott/Dialogues">Dialogues</a></dd>
</dl>
<h2><span class="mw-headline" id="Citations_des_bandes_dessin.C3.A9es">Citations des bandes dessinées</span></h2>
<dl>
<dd><i>Voir le recueil de citations&#160;:</i> <a href="/wiki/Kaamelott/Bandes_dessin%C3%A9es" title="Kaamelott/Bandes dessinées">Bandes dessinées</a></dd>
</dl>
<div class="noprint" style="clear:right;float:right;margin:1em 0 1em 1em;width:250px;border:solid #aaaaaa 1px;background:#f9f9f9;padding:4px;font-size:90%;text-align:left;">
<p>Vous pouvez également consulter les articles suivants sur les autres projets <a href="https://meta.wikimedia.org/wiki/Accueil" class="extiw" title="m:Accueil">Wikimédia</a>&#160;:</p>
<div style="clear:left;padding:1px">
<div style="float:left;margin:1px">
<div style="position:relative;width:25px;height:34px;z-index:2;overflow:hidden;"><a href="/wiki/Fichier:Commons-logo.svg" class="image"><img alt="Commons-logo.svg" src="//upload.wikimedia.org/wikipedia/commons/thumb/4/4a/Commons-logo.svg/25px-Commons-logo.svg.png" width="25" height="34" srcset="//upload.wikimedia.org/wikipedia/commons/thumb/4/4a/Commons-logo.svg/38px-Commons-logo.svg.png 1.5x, //upload.wikimedia.org/wikipedia/commons/thumb/4/4a/Commons-logo.svg/50px-Commons-logo.svg.png 2x" data-file-width="1024" data-file-height="1376" /></a>
<div class="nodeco" style="position:absolute;top:0;left:0;padding-top:3px;z-index:3;"><a href="https://commons.wikimedia.org/wiki/Kaamelott" class="extiw" title="commons:Kaamelott"><span style="float:left;width:25px;height:34px;font-size:34px;line-height:34px;word-spacing:25px;cursor:hand;">&#160; &#160;</span></a></div>
</div>
</div>
<p><i><b><a href="https://commons.wikimedia.org/wiki/Kaamelott" class="extiw" title="commons:Kaamelott">Ressources multimédia</a></b></i> sur <a href="https://commons.wikimedia.org/wiki/Accueil" class="extiw" title="commons:Accueil">Commons</a>.</p>
</div>
<div style="clear:left;padding:1px">
<div style="float:left;margin:1px">
<div style="position:relative;width:25px;height:30px;z-index:2;overflow:hidden;"><a href="/wiki/Fichier:Wikipedia-logo.png" class="image"><img alt="Wikipedia-logo.png" src="//upload.wikimedia.org/wikipedia/commons/thumb/6/63/Wikipedia-logo.png/25px-Wikipedia-logo.png" width="25" height="25" srcset="//upload.wikimedia.org/wikipedia/commons/thumb/6/63/Wikipedia-logo.png/38px-Wikipedia-logo.png 1.5x, //upload.wikimedia.org/wikipedia/commons/thumb/6/63/Wikipedia-logo.png/50px-Wikipedia-logo.png 2x" data-file-width="200" data-file-height="200" /></a>
<div class="nodeco" style="position:absolute;top:0;left:0;padding-top:3px;z-index:3;"><a href="https://fr.wikipedia.org/wiki/Kaamelott" class="extiw" title="w:Kaamelott"><span style="float:left;width:25px;height:30px;font-size:30px;line-height:30px;word-spacing:25px;cursor:hand;">&#160; &#160;</span></a></div>
</div>
</div>
<p><i><b><a href="https://fr.wikipedia.org/wiki/Kaamelott" class="extiw" title="w:Kaamelott">Article</a></b></i> sur <a href="https://fr.wikipedia.org/wiki/" class="extiw" title="w:">Wikipédia</a>.</p>
</div>
</div>


<!-- 
NewPP limit report
Parsed by mw1202
Cached time: 20170602124454
Cache expiry: 2592000
Dynamic content: false
CPU time usage: 0.476 seconds
Real time usage: 0.509 seconds
Preprocessor visited node count: 18307/1000000
Preprocessor generated node count: 0/1500000
Post\u2010expand include size: 50705/2097152 bytes
Template argument size: 45650/2097152 bytes
Highest expansion depth: 11/40
Expensive parser function count: 0/500
Lua time usage: 0.007/10.000 seconds
Lua memory usage: 558 KB/50 MB
-->
<!--
Transclusion expansion time report (%,ms,calls,template)
100.00%  350.452      1 -total
 71.92%  252.047    132 Mod\u00e8le:R\u00e9f_S\u00e9rie
 57.87%  202.823    132 Mod\u00e8le:R\u00e9f_impr\u00e9cise
 51.88%  181.810    132 Mod\u00e8le:Print
 44.82%  157.087    132 Mod\u00e8le:Concat10
  9.09%   31.855      1 Mod\u00e8le:Titre_en_italique
  7.85%   27.523    132 Mod\u00e8le:Citation
  2.43%    8.505     28 Mod\u00e8le:Loupe
  1.58%    5.538      1 Mod\u00e8le:Autres_projets
-->
</div>
<!-- Saved in parser cache with key frwikiquote:pcache:idhash:4012-0!*!0!!fr!4!* and timestamp 20170602124654 and revision id 254765
 -->
`


export default expected_fr_kaamelott_html
