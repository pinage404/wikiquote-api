const expected_fr_kaamelott_yvain_html = `<p>Citations de <b><a href="https://fr.wikipedia.org/wiki/Personnages_de_Kaamelott#Yvain" class="extiw" title="w:Personnages de Kaamelott">Yvain</a></b> dans la <a href="/wiki/Cat%C3%A9gorie:S%C3%A9rie_t%C3%A9l%C3%A9vis%C3%A9e" title="Catégorie:Série télévisée">série télévisée</a> <a href="/wiki/Kaamelott" title="Kaamelott">Kaamelott</a>.</p>
<h2><span class="mw-headline" id="Citations">Citations</span></h2>
<p><span class="citation">Je refuse d’aller me battre pour soutenir une politique d’expansion territoriale dont je ne reconnais pas la légitimité.</span></p>
<ul>
<li>
<div class="ref">Simon Astier, <i>Kaamelott</i>, Livre I, 39&#160;: <i>Le Cas Yvain</i>, écrit par Alexandre Astier.</div>
</li>
</ul>
<p><br /></p>
<p><span class="citation">(À Séli) Je peux pas y aller j’ai une otite.</span></p>
<ul>
<li>
<div class="ref">Simon Astier, <i>Kaamelott</i>, Livre I, 39&#160;: <i>Le cas Yvain</i>, écrit par Alexandre Astier.</div>
</li>
</ul>
<p><br /></p>
<p><span class="citation">Est-ce qu’on a le droit de boire du cidre&#160;?</span></p>
<ul>
<li>
<div class="ref">Simon Astier, <i>Kaamelott</i>, Livre I, 94&#160;: <i>Les Nouveaux Frères</i>, écrit par Alexandre Astier.</div>
</li>
</ul>
<p><br /></p>
<p><span class="citation">Je le prends pas le bouclier, ça fait trop nul&#160;!</span></p>
<ul>
<li>
<div class="ref">Simon Astier, <i>Kaamelott</i>, Livre I, 94&#160;: <i>Les Nouveaux Frères</i>, écrit par Alexandre Astier.</div>
</li>
</ul>
<p><br /></p>
<p><span class="citation">Mon père, y dit toujours qu'on arrive jamais en prison par hasard.</span></p>
<ul>
<li>
<div class="ref">Simon Astier, <i>Kaamelott</i>, Livre II, 18&#160;: <i>Sous les verrous</i>, écrit par Alexandre Astier.</div>
</li>
</ul>
<p><br /></p>
<p><span class="citation">Moi, je m'en fous, si on me force à y retourner, je retiens ma respiration jusqu'à ce qu'on arrête de me forcer à y retourner.</span></p>
<ul>
<li>
<div class="ref">Simon Astier, <i>Kaamelott</i>, Livre II, 24&#160;: <i>Les tuteurs</i>, écrit par Alexandre Astier.</div>
</li>
</ul>
<p><br /></p>
<p><span class="citation">Quand il a ouvert le ventre du troll, j'ai reçu un jet d'acide qui m'a pratiquement acidifié&#160;!</span></p>
<ul>
<li>
<div class="ref">Simon Astier, <i>Kaamelott</i>, Livre II, 24&#160;: <i>Les tuteurs</i>, écrit par Alexandre Astier.</div>
</li>
</ul>
<p><br /></p>
<p><span class="citation">Moi, je me suis fait dérober de l'alimentation tout le long du voyage&#160;! Une véritable dérobade&#160;!</span></p>
<ul>
<li>
<div class="ref">Simon Astier, <i>Kaamelott</i>, Livre II, 25&#160;: <i>Les tuteurs</i>, écrit par Alexandre Astier.</div>
</li>
</ul>
<p><br /></p>
<p><span class="citation">Et moi, j'ai sympathisé avec un crémier qui m'a expliqué les tenants et les aboutissants de sa profession avec beaucoup de simplicité&#160;!</span></p>
<ul>
<li>
<div class="ref">Simon Astier, <i>Kaamelott</i>, Livre II, 24&#160;: <i>Les tuteurs</i>, écrit par Alexandre Astier.</div>
</li>
</ul>
<p><br /></p>
<p><span class="citation">J’étais tellement en colère, je leur ai lancé un de ces regards… Ils sont pas venus chercher la monnaie de leur pièce.</span></p>
<ul>
<li>
<div class="ref">Simon Astier, <i>Kaamelott</i>, Livre II, 29&#160;: <i>Les Mauvaises Graines</i>, écrit par Alexandre Astier.</div>
</li>
</ul>
<p><br /></p>
<p><span class="citation">Ben Gauvain il est revenu, il s'était planté une écharde comme ça dans le doigt… N’empêche, si ça s’infecte, on peut en mourir, j’vous ferais dire.</span></p>
<ul>
<li>
<div class="ref">Simon Astier, <i>Kaamelott</i>, Livre II, 52&#160;: <i>Le Guet</i>, écrit par Alexandre Astier.</div>
</li>
</ul>
<p><br /></p>
<p><span class="citation">La colombe blanche et sèche,<br />
Retombe souvent sur sa poitrine,<br />
Elle vole quand même... c'est très bien,<br />
Et finit sa course dans la prairie de notre enfance. […]<br />
Là sire j'avoue que c'est pour que ça rime avec blanche et sèche. […]<br />
Ben oui c'est une rime triple&#160;: Blanche et sèche, poitrine et prairie de notre enfance.</span></p>
<ul>
<li>
<div class="ref">Simon Astier, <i>Kaamelott</i>, Livre II, 86&#160;: <i>Les félicitations</i>, écrit par Alexandre Astier.</div>
</li>
</ul>
<p><br /></p>
<p><span class="citation">J’estime ne pas avoir à subir les fantasmes carriéristes d’une entité générationnelle réactionnaire et oppressive&#160;!</span></p>
<ul>
<li>
<div class="ref">Simon Astier, <i>Kaamelott</i>, Livre II, 95&#160;: .<i>Le Pédagogue</i>, écrit par Alexandre Astier.</div>
</li>
</ul>
<p><br /></p>
<p><span class="citation">Si je fais mon tour pas dans le même sens que vous, ça annule. Mais si je le fais dans le même sens que vous, ça le double&#160;!</span></p>
<ul>
<li>
<div class="ref">Simon Astier, <i>Kaamelott</i>, Livre II, 97&#160;: <i>Trois Cent Soixante Degrés</i>, écrit par Alexandre Astier.</div>
</li>
</ul>
<p><br /></p>
<p><span class="citation">[Arthur: Pour faire simple: on peut douter de tout sauf de la nécessité de se trouver du côté des opprimés]<br />
Ah bon! C'est marrant parce que mon père, il a une phrase presque pareil: "On peut douter de tout sauf de la nécessité de se trouver du côté de celui qui a le pognon".</span></p>
<ul>
<li>
<div class="ref">Simon Astier, <i>Kaamelott</i>, Livre III, 91&#160;: <i>La Chevalerie</i>, écrit par Alexandre Astier.</div>
</li>
</ul>
<p><br /></p>
<p><span class="citation">La chevalerie, c'est pas là où on range les chevaux&#160;?</span></p>
<ul>
<li>
<div class="ref">Simon Astier, <i>Kaamelott</i>, Livre III, 91&#160;: <i>La Chevalerie</i>, écrit par Alexandre Astier.</div>
</li>
</ul>
<p><br /></p>
<p><span class="citation">(Gauvain: Excusez nous mon oncle, notre vocabulaire n'est pas tres fourni)</span></p>
<p><span class="citation">Ouais bah vous encore ça va, moi, même le mot vocabulaire je suis pas sûr à 100%&#160;!</span></p>
<ul>
<li>
<div class="ref">Simon Astier, <i>Kaamelott</i>, Livre III, 91&#160;: <i>La Chevalerie</i>, écrit par Alexandre Astier.</div>
</li>
</ul>
<p><br /></p>
<p><span class="citation">(<i>À propos d'une catapulte</i>) Est-ce qu'on peut s'en servir pour donner de l'élan à un pigeon&#160;?</span></p>
<ul>
<li>
<div class="ref">Simon Astier, <i>Kaamelott</i>, Livre III, 88&#160;: <i>L'étudiant</i>, écrit par Alexandre Astier.</div>
</li>
</ul>
<p><br /></p>
<p><span class="citation">En revanche, je suis triste pour un autre truc mais ça m'embête d'en parler... J'estime que chacun a le droit à son jardin secret.</span></p>
<ul>
<li>
<div class="ref">Simon Astier, <i>Kaamelott</i>, Livre IV, 05&#160;: <i>La tarte aux fraises</i>, écrit par Alexandre Astier.</div>
</li>
</ul>
<p><br /></p>
<p><span class="citation">Je crois que je me suis coupé la gencive avec un grumeau cuit. C'est possible ou pas&#160;?</span></p>
<ul>
<li>
<div class="ref">Simon Astier, <i>Kaamelott</i>, Livre IV, 05&#160;: <i>La tarte aux fraises</i>, écrit par Alexandre Astier.</div>
</li>
</ul>
<p><br /></p>
<p><span class="citation">Dans ma chambre c'est le bordel, ca pue et il y a des charençons.</span></p>
<ul>
<li>
<div class="ref">Simon Astier, <i>Kaamelott</i>, Livre IV, 22&#160;: <i>Seigneur Caïus</i>, écrit par Alexandre Astier.</div>
</li>
</ul>
<p><br /></p>
<p><span class="citation">Et si on envoyait quelqu’un qui se faufile dans leur camp sans se faire repérer pour observer leurs faits et gestes&#160;? […] Ou alors, ça fait doublon avec l’espionnage&#160;? Je me rends pas compte.</span></p>
<ul>
<li>
<div class="ref">Simon Astier, <i>Kaamelott</i>, Livre IV, 32&#160;: <i>Le Rapport</i>, écrit par Alexandre Astier.</div>
</li>
</ul>
<p><br /></p>
<p><span class="citation">Parce que en fait dans notre langue, il y a que deux mots qui riment avec complète&#160;: c'est quiquette et biseautée. Alors ça va être chaud&#160;!</span></p>
<ul>
<li>
<div class="ref">Simon Astier, <i>Kaamelott</i>, Livre IV, 73&#160;: <i>Le Oud II</i>, écrit par Alexandre Astier.</div>
</li>
</ul>
<p><br /></p>
<p><span class="citation">Ne vous laissez pas embobiner… Ils cherchent à vous rembobiner&#160;!</span></p>
<ul>
<li>
<div class="ref">Simon Astier, <i>Kaamelott</i>, Livre IV, 76&#160;: <i>Le Choix de Gauvain</i>, écrit par Alexandre Astier.</div>
</li>
</ul>
<p><br /></p>
<p><span class="citation">Il s'est acharné sur moi avec une sauvagerie mêlée de férocité. […] Celui qu'avait qu'une jambe. […] Oui, bah du coup, c'était le plus hargneux. C'était comme s'il se vengeait sur moi de sa jambe euh... Avec une sauvagerie mêlée de férocité.</span></p>
<ul>
<li>
<div class="ref">Simon Astier, <i>Kaamelott</i>, Livre IV, 81&#160;: <i>La Blessure d'Yvain</i>, écrit par Alexandre Astier.</div>
</li>
</ul>
<p><br /></p>
<p><span class="citation">Ça pique trop de la vie, quoi&#160;: ça pique, ça lance, et derrière, comment ça re-pique trop.</span></p>
<ul>
<li>
<div class="ref">Simon Astier, <i>Kaamelott</i>, Livre IV, 81&#160;: <i>La Blessure d'Yvain</i>, écrit par Alexandre Astier.</div>
</li>
</ul>
<p><br /></p>
<p><span class="citation">Vous pouvez pas savoir comment on est trop jouasse d’être là&#160;!</span></p>
<ul>
<li>
<div class="ref">Simon Astier, <i>Kaamelott</i>, Livre V, 4&#160;: <i>Les Nouveaux Clans</i>, écrit par Alexandre Astier.</div>
</li>
</ul>
<p><br /></p>
<p><span class="citation">Ouais, alors là où je vois que je suis vraiment à cran, c'est que quand vous avez dit malédiction, j'ai fait une série de tout petits pets comme ça&#160;: pft pft pft pft pft pft.</span></p>
<ul>
<li>
<div class="ref">Simon Astier, <i>Kaamelott</i>, Livre V, 11&#160;: <i>Hurlements</i>, écrit par Alexandre Astier.</div>
</li>
</ul>
<p><br /></p>
<p><span class="citation">Ah bah ouais mais là euh... Le voile noir, le chaos... J'ai passé un cap hein.</span></p>
<ul>
<li>
<div class="ref">Simon Astier, <i>Kaamelott</i>, Livre V, 11&#160;: <i>Hurlements</i>, écrit par Alexandre Astier.</div>
</li>
</ul>
<p><br /></p>
<p><br /></p>
<p><span class="citation">Mais c'est trop sur votre route quoi&#160;! Bon y'a une colline, deux collines du coup bi-collines et après c'est carrément votre route&#160;!</span></p>
<ul>
<li>
<div class="ref">Simon Astier, <i>Kaamelott</i>, Livre V, 40&#160;: <i>Les Pionniers</i>, écrit par Alexandre Astier.</div>
</li>
</ul>
<p><br /></p>
<p><span class="citation">Femme, cesse donc de nous esbaudir les oreilles&#160;! Olala, il suffit&#160;!</span></p>
<ul>
<li>
<div class="ref">Simon Astier, <i>Kaamelott</i>, Livre V, 40&#160;: <i>Les Pionniers</i>, écrit par Alexandre Astier.</div>
</li>
</ul>
<p><br /></p>
<p><span class="citation">Ah bravo femme. Ce repas fut des plus roboratif&#160;!</span></p>
<ul>
<li>
<div class="ref">Simon Astier, <i>Kaamelott</i>, Livre V, 40&#160;: <i>Les pionniers</i>, écrit par Alexandre Astier.</div>
</li>
</ul>
<p><br /></p>
<p><span class="citation">Femme, comme je le disais tantôt,tu nous a roboré. Mais sauras tu nous régaler d'un dessert de ton choix&#160;?</span></p>
<ul>
<li>
<div class="ref">Simon Astier, <i>Kaamelott</i>, Livre V, 40&#160;: <i>Les pionniers</i>, écrit par Alexandre Astier.</div>
</li>
</ul>
<p><br /></p>
<p><span class="citation">Allez femme, robore nous donc de quelques fruits bien sucrés. Il s'agit d'une régalade&#160;!</span></p>
<ul>
<li>
<div class="ref">Simon Astier, <i>Kaamelott</i>, Livre V, 40&#160;: <i>Les pionniers</i>, écrit par Alexandre Astier.</div>
</li>
</ul>
<p><br /></p>
<p><span class="citation">Femme&#160;! Tu nous abaubis les oreilles&#160;! Rabats un peu ta casquette&#160;!</span></p>
<ul>
<li>
<div class="ref">Simon Astier, <i>Kaamelott</i>, Livre V, 40&#160;: <i>Les Pionniers</i>, écrit par Alexandre Astier.</div>
</li>
</ul>
<p><br /></p>
<p><span class="citation">Oh l'Irlande&#160;!</span></p>
<ul>
<li>
<div class="ref">Simon Astier, <i>Kaamelott</i>, Livre V, 21: <i>Aux Yeux De Tous III</i>, écrit par Alexandre Astier.</div>
</li>
</ul>
<p><br /></p>
<div class="noprint" style="clear:right;float:right;margin:1em 0 1em 1em;width:250px;border:solid #aaaaaa 1px;background:#f9f9f9;padding:4px;font-size:90%;text-align:left;">
<p>Vous pouvez également consulter les articles suivants sur les autres projets <a href="https://meta.wikimedia.org/wiki/Accueil" class="extiw" title="m:Accueil">Wikimédia</a>&#160;:</p>
<div style="clear:left;padding:1px">
<div style="float:left;margin:1px">
<div style="position:relative;width:25px;height:30px;z-index:2;overflow:hidden;"><a href="/wiki/Fichier:Wikipedia-logo.png" class="image"><img alt="Wikipedia-logo.png" src="//upload.wikimedia.org/wikipedia/commons/thumb/6/63/Wikipedia-logo.png/25px-Wikipedia-logo.png" width="25" height="25" srcset="//upload.wikimedia.org/wikipedia/commons/thumb/6/63/Wikipedia-logo.png/38px-Wikipedia-logo.png 1.5x, //upload.wikimedia.org/wikipedia/commons/thumb/6/63/Wikipedia-logo.png/50px-Wikipedia-logo.png 2x" data-file-width="200" data-file-height="200" /></a>
<div class="nodeco" style="position:absolute;top:0;left:0;padding-top:3px;z-index:3;"><a href="https://fr.wikipedia.org/wiki/Personnages_de_Kaamelott#Yvain" class="extiw" title="w:Personnages de Kaamelott"><span style="float:left;width:25px;height:30px;font-size:30px;line-height:30px;word-spacing:25px;cursor:hand;">&#160; &#160;</span></a></div>
</div>
</div>
<p><i><b><a href="https://fr.wikipedia.org/wiki/Personnages_de_Kaamelott#Yvain" class="extiw" title="w:Personnages de Kaamelott">Article</a></b></i> sur <a href="https://fr.wikipedia.org/wiki/" class="extiw" title="w:">Wikipédia</a>.</p>
</div>
</div>


<!-- 
NewPP limit report
Parsed by mw1181
Cached time: 20170508211148
Cache expiry: 2592000
Dynamic content: false
CPU time usage: 0.064 seconds
Real time usage: 0.077 seconds
Preprocessor visited node count: 4739/1000000
Preprocessor generated node count: 0/1500000
Post\u2010expand include size: 11416/2097152 bytes
Template argument size: 9745/2097152 bytes
Highest expansion depth: 11/40
Expensive parser function count: 0/500
-->
<!--
Transclusion expansion time report (%,ms,calls,template)
100.00%   52.320      1 -total
 78.13%   40.880     36 Mod\u00e8le:R\u00e9f_S\u00e9rie
 61.84%   32.352     36 Mod\u00e8le:R\u00e9f_impr\u00e9cise
 55.04%   28.797     36 Mod\u00e8le:Print
 46.00%   24.069     36 Mod\u00e8le:Concat10
  9.25%    4.840     37 Mod\u00e8le:Citation
  6.50%    3.399      1 Mod\u00e8le:Autres_projets
-->

<!-- Saved in parser cache with key frwikiquote:pcache:idhash:17590-0!*!0!*!*!4!* and timestamp 20170508211148 and revision id 252767
 -->
`


export default expected_fr_kaamelott_yvain_html
