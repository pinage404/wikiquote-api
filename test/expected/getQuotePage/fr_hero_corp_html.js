const expected_fr_hero_corp_html = `<div class="mw-parser-output"><p><i><b><a href="https://fr.wikipedia.org/wiki/fr:Hero_Corp" class="extiw" title="w:fr:Hero Corp">Hero Corp</a></b></i> est une <a href="https://fr.wikipedia.org/wiki/fr:s%C3%A9rie_t%C3%A9l%C3%A9vis%C3%A9e" class="extiw" title="w:fr:série télévisée">série télévisée</a> <a href="https://fr.wikipedia.org/wiki/France" class="extiw" title="w:France">française</a> humoristique et dramatique de <a href="https://fr.wikipedia.org/wiki/fr:science-fiction" class="extiw" title="w:fr:science-fiction">science-fiction</a>, créée par <a href="https://fr.wikipedia.org/wiki/fr:Simon_Astier" class="extiw" title="w:fr:Simon Astier">Simon Astier</a> et diffusée depuis le <a href="https://fr.wikipedia.org/wiki/25_octobre" class="extiw" title="w:25 octobre">25</a> <a href="https://fr.wikipedia.org/wiki/octobre_2008" class="extiw" title="w:octobre 2008">octobre</a> <a href="https://fr.wikipedia.org/wiki/2008" class="extiw" title="w:2008">2008</a> sur <a href="https://fr.wikipedia.org/wiki/fr:Com%C3%A9die_!" class="extiw" title="w:fr:Comédie !">Comédie !</a> et depuis le <a href="https://fr.wikipedia.org/wiki/5_juillet" class="extiw" title="w:5 juillet">5</a> <a href="https://fr.wikipedia.org/wiki/juillet_2009" class="extiw" title="w:juillet 2009">juillet</a> <a href="https://fr.wikipedia.org/wiki/2009" class="extiw" title="w:2009">2009</a> sur <a href="https://fr.wikipedia.org/wiki/fr:France_4" class="extiw" title="w:fr:France 4">France 4</a>. La deuxième saison est diffusée sur <a href="https://fr.wikipedia.org/wiki/fr:Com%C3%A9die_!" class="extiw" title="w:fr:Comédie !">Comédie !</a> depuis le <a href="https://fr.wikipedia.org/wiki/8_janvier" class="extiw" title="w:8 janvier">8</a> <a href="https://fr.wikipedia.org/wiki/janvier_2010" class="extiw" title="w:janvier 2010">janvier</a> <a href="https://fr.wikipedia.org/wiki/2010_%C3%A0_la_t%C3%A9l%C3%A9vision" class="extiw" title="w:2010 à la télévision">2010</a>.</p>
<p></p>
<h2><span class="mw-headline" id="Saison_1">Saison 1</span></h2>
<h3><span class="mw-headline" id=".C3.89pisode_1_:_Le_village">Épisode 1&#160;: Le village</span></h3>
<div class="poem">
<p><span class="citation"><span class="personnage">Steve</span>&#160;: Bonjour John.<br />
<span class="personnage">John</span>&#160;: Ah la vache vous m'avez fait peur&#160;!<br />
<span class="personnage">Steve</span>&#160;: Vous avez fait bon voyage&#160;?<br />
<span class="personnage">John</span>&#160;: Carrément pas&#160;! On est où ici&#160;?<br />
<span class="personnage">Steve</span>&#160;: On est loin de chez vous&#160;!</span></p>
</div>
<ul>
<li>
<div class="ref">Gérard Darier, Simon Astier, <i>Hero Corp</i>, saison 1, épisode 1 (<i>Le village</i>), écrit par Simon Astier, première diffusion par Comédie&#160;!.</div>
</li>
</ul>
<p><br /></p>
<div class="poem">
<p><span class="citation"><span class="personnage">John</span>&#160;: Vous avez un brief general au niveau de l'accueil, non&#160;?<br />
<span class="personnage">Steve</span>&#160;: Pourquoi&#160;?<br />
<span class="personnage">John</span>&#160;: Non, je sais pas, ya comme une cohérence je trouve.</span></p>
</div>
<ul>
<li>
<div class="ref">Gérard Darier, Simon Astier, <i>Hero Corp</i>, saison 1, épisode 1 (<i>Le village</i>), écrit par Simon Astier, première diffusion par Comédie&#160;!.</div>
</li>
</ul>
<p><br /></p>
<p><span class="citation"><span class="personnage">Steve</span>&#160;: Comme on a tous pas mal de boulot ici...<br />
<span class="personnage">John</span>&#160;: Ici?! Vous avez trop de boulot ici?! Mais qu'est ce que vous faites de vos journées, vous vous greffez des organes de porcs?!</span></p>
<ul>
<li>
<div class="ref">Gérard Darier, Simon Astier, <i>Hero Corp</i>, saison 1, épisode 1 (<i>Le village</i>), écrit par Simon Astier, première diffusion par Comédie&#160;!.</div>
</li>
</ul>
<p><br /></p>
<p><span class="citation"><span class="personnage">John</span>&#160;: On peut s'faire une omelette sinon...<br />
<span class="personnage">Klaus</span>&#160;: Quand t'aura bouffé un de mes cochons tu comprendra que tes omelettes, le poulet, les entrecôtes, tout ça c'est pour les conasses&#160;!</span></p>
<ul>
<li>
<div class="ref">Alban Lenoir, Simon Astier, <i>Hero Corp</i>, saison 1, épisode 1 (<i>Le village</i>), écrit par Simon Astier, première diffusion par Comédie&#160;!.</div>
</li>
</ul>
<p><br /></p>
<h3><span class="mw-headline" id=".C3.89pisode_2_:_Le_test">Épisode 2&#160;: Le test</span></h3>
<div class="poem">
<p><span class="citation"><span class="personnage">Allen</span>&#160;: Alors&#160;? T'en penses quoi du petit&#160;?<br />
<span class="personnage">Klaus</span>&#160;: Qui&#160;?<br />
<span class="personnage">Allen</span>&#160;: Bah le petit.<br />
<span class="personnage">Klaus</span>&#160;: Précise un peu y'en a plein des mecs petits dans le village.<br />
<span class="personnage">Allen</span>&#160;: Des mecs qu'on fait venir pour sauver le monde, t'en connais beaucoup toi&#160;?<br />
<span class="personnage">Klaus</span>&#160;: Ah ouais c'est vrai... Mais je trouve pas qu'il soit petit, moi.<br />
<span class="personnage">Allen</span>&#160;: Alors t'en penses quoi&#160;?<br />
<span class="personnage">Klaus</span>&#160;: Moi je dirais ... moyen ... ouais c'est ça il est de taille moyenne.<br />
<span class="personnage">Allen</span>&#160;: Alors&#160;?<br />
<span class="personnage">Klaus</span>&#160;: Non&#160;! Moyen grand&#160;! voilà c'est ça, il est moyen-grand.</span></p>
</div>
<ul>
<li>
<div class="ref">Maurice Lamy, Alban Lenoir, <i>Hero Corp</i>, saison 1, épisode 2 (<i>Le test</i>), écrit par Simon Astier, première diffusion par Comédie&#160;!.</div>
</li>
</ul>
<p><br /></p>
<div class="poem">
<p><span class="citation"><span class="personnage">John</span>&#160;: Et vous faites quoi attaché là&#160;?<br />
<span class="personnage">Le prisonnier</span>&#160;: Je fais sécher du jambon.</span></p>
</div>
<ul>
<li>
<div class="ref">Simon Astier, Christian Bujeau, <i>Hero Corp</i>, saison 1, épisode 2 (<i>Le test</i>), écrit par Simon Astier, première diffusion par Comédie&#160;!.</div>
</li>
</ul>
<p><br /></p>
<div class="poem">
<p><span class="citation"><span class="personnage">Jennifer</span>&#160;: Bon, on dormira ensemble un autre soir... (silence) Euh, on n'est pas encore assez proches pour ce genre de blagues&#160;?<br />
<span class="personnage">John</span>&#160;: Ça passe.</span></p>
</div>
<ul>
<li>
<div class="ref">Aurore Pourteyron, Simon Astier, <i>Hero Corp</i>, saison 1, épisode 2 (<i>Le test</i>), écrit par Simon Astier, première diffusion par Comédie&#160;!.</div>
</li>
</ul>
<p><br /></p>
<div class="poem">
<p><span class="citation"><span class="personnage">John</span>&#160;: Et... comment on fait pour que vous mangiez&#160;?<br />
<span class="personnage">Le prisonnier</span>&#160;: Ça dépend... vous savez bien viser&#160;?</span></p>
</div>
<ul>
<li>
<div class="ref">Simon Astier, Christian Bujeau, <i>Hero Corp</i>, saison 1, épisode 2 (<i>Le test</i>), écrit par Simon Astier, première diffusion par Comédie&#160;!.</div>
</li>
</ul>
<p><br /></p>
<h3><span class="mw-headline" id=".C3.89pisode_3_:_Le_grand_d.C3.A9part">Épisode 3&#160;: Le grand départ</span></h3>
<div class="poem">
<p><span class="citation"><span class="personnage">Mary</span>&#160;: De "Acid Man" classe 80, Burt devient "Captain Shampooing" classe 136.</span></p>
</div>
<ul>
<li>
<div class="ref">Agnès Boury, <i>Hero Corp</i>, saison 1, épisode 3 (<i>Le grand départ</i>), écrit par Simon Astier, première diffusion par Comédie&#160;!.</div>
</li>
</ul>
<p><br /></p>
<div class="poem">
<p><span class="citation"><span class="personnage">John</span>&#160;: Vous auriez pas des quiches ou des trucs dans le genre&#160;?<br />
<span class="personnage">Steve</span>&#160;: C'est une boulangerie ici, pas une quicherie.</span></p>
</div>
<ul>
<li>
<div class="ref">Simon Astier, Gérard Darier, <i>Hero Corp</i>, saison 1, épisode 3 (<i>Le grand départ</i>), écrit par Simon Astier, première diffusion par Comédie&#160;!.</div>
</li>
</ul>
<p><br /></p>
<div class="poem">
<p><span class="citation"><span class="personnage">The Lord</span>&#160;: On ne part pas les mains vides&#160;!</span></p>
</div>
<ul>
<li>
<div class="ref">Christian Bujeau, <i>Hero Corp</i>, saison 1, épisode 3 (<i>Le grand départ</i>), écrit par Simon Astier, première diffusion par Comédie&#160;!.</div>
</li>
</ul>
<p><br /></p>
<div class="poem">
<p><span class="citation"><span class="personnage">Klaus</span>&#160;: Si t'as besoin d'aide...<br />
<span class="personnage">John</span>&#160;: Je t'appelle...&#160;?<br />
<span class="personnage">Klaus</span>&#160;: Non j'voulais dire que si t'as besoin d'aide ça va être chaud pour ta gueule parce que tout le monde dort à cette heure-ci et que ma baraque est à l'autre bout du village.</span></p>
</div>
<ul>
<li>
<div class="ref">Alban Lenoir, Simon Astier, <i>Hero Corp</i>, saison 1, épisode 3 (<i>Le grand départ</i>), écrit par Simon Astier, première diffusion par Comédie&#160;!.</div>
</li>
</ul>
<p><br /></p>
<h3><span class="mw-headline" id=".C3.89pisode_4_:_R.C3.A9v.C3.A9lations">Épisode 4&#160;: Révélations</span></h3>
<div class="poem">
<p><span class="citation"><span class="personnage">Mary</span>&#160;: C'est pas tout.<br />
<span class="personnage">John</span>&#160;: De quoi&#160;? (sarcastique) John c'est pas mon vrai prénom, mes parents étaient des super-héros, l'un des deux était méchant donc c'est pour ça que je suis au courant de rien et qu'on m'a confié à toi&#160;?<br />
<span class="personnage">Mary</span>&#160;: Ah bah ça fait déjà ça de moins à expliquer.</span></p>
</div>
<ul>
<li>
<div class="ref">Agnès Boury, Simon Astier, <i>Hero Corp</i>, saison 1, épisode 4 (<i>Révélations</i>), écrit par Simon Astier, première diffusion par Comédie&#160;!.</div>
</li>
</ul>
<p><br /></p>
<div class="poem">
<p><span class="citation"><span class="personnage">Mary</span>&#160;: Je suis une super-héros.<br />
<span class="personnage">John</span>&#160;: On dit "une super-héros"&#160;?<br />
<span class="personnage">Mary</span>&#160;: On s'est assez battues pour, ouais.</span></p>
</div>
<ul>
<li>
<div class="ref">Agnès Boury, Simon Astier, <i>Hero Corp</i>, saison 1, épisode 4 (<i>Révélations</i>), écrit par Simon Astier, première diffusion par Comédie&#160;!.</div>
</li>
</ul>
<p><br /></p>
<div class="poem">
<p><span class="citation"><span class="personnage">John</span>&#160;: Je suis dans un village de super-héros moisis. Tu m’as fait venir dans un village de super-héros moisis&#160;! Mais si j’raconte ça quand j’rentre on va m’lancer des cailloux quoi&#160;!</span></p>
</div>
<ul>
<li>
<div class="ref">Simon Astier, <i>Hero Corp</i>, saison 1, épisode 4 (<i>Révélations</i>), écrit par Simon Astier, première diffusion par Comédie&#160;!.</div>
</li>
</ul>
<p><br /></p>
<h3><span class="mw-headline" id=".C3.89pisode_8_:_Nouvelle_donne">Épisode 8&#160;: Nouvelle donne</span></h3>
<div class="poem">
<p><span class="citation"><span class="personnage">Cécil (le maire)</span>&#160;: Toi Klaus tu seras le chef du groupe des...<br />
<span class="personnage">Klaus</span>&#160;: Des enculés&#160;?!<br />
<span class="personnage">John</span>&#160;: Je sais pas pourquoi mais je suis sûr que je suis dedans.<br />
<span class="personnage">Cécil</span>&#160;: Exactement.<br />
<span class="personnage">Klaus</span>&#160;: C'est pas mal ça comme nom, ça sonne.</span></p>
</div>
<ul>
<li>
<div class="ref">Philippe Noël, Simon Astier, Alban Lenoir, <i>Hero Corp</i>, saison 1, épisode 8 (<i>Nouvelle donne</i>), écrit par Simon Astier, première diffusion par Comédie&#160;!.</div>
</li>
</ul>
<p><br /></p>
<h3><span class="mw-headline" id=".C3.89pisode_9_:_Emplettes">Épisode 9&#160;: Emplettes</span></h3>
<div class="poem">
<p><span class="citation"><span class="personnage">Mique</span>&#160;: Qui est là? Je vous entends penser, qui est là&#160;? (silence) Vous venez de dire que je devais passer sous un pont&#160;!?<br />
<span class="personnage">Laurence</span>&#160;: Euh non.<br />
<span class="personnage">Mique</span>&#160;: Ah bon&#160;?<br />
<span class="personnage">Laurence</span>&#160;: Non, j'ai dis que vous aviez l'air d'un con.</span></p>
</div>
<ul>
<li>
<div class="ref">Étienne Fague, (voix de) Didier Bénureau, <i>Hero Corp</i>, saison 1, épisode 9 (<i>Emplettes</i>), écrit par Simon Astier, première diffusion par Comédie&#160;!.</div>
</li>
</ul>
<p><br /></p>
<h3><span class="mw-headline" id=".C3.89pisode_10_:_L.27alerte">Épisode 10&#160;: L'alerte</span></h3>
<div class="poem">
<p><span class="citation"><span class="personnage">Klaus</span>&#160;: Mais c'est la bouffe, elle est empoisonnée&#160;!<br />
<span class="personnage">Doug</span>&#160;: Merde&#160;!! J'ai donné des cartons à Allen&#160;!<br />
<span class="personnage">Klaus</span>&#160;: Bouge pas, surtout, ne bouge pas&#160;!<br />
<span class="personnage">John</span>&#160;: Pourquoi&#160;?<br />
<span class="personnage">Klaus</span>&#160;: A cause du poison, il faut pas qu'le sang circule. Tu peux bloquer ton sang&#160;?<br />
<span class="personnage">John</span>&#160;: Ah ben non, excuse moi, non&#160;!<br />
<span class="personnage">Klaus</span>&#160;: Ben non mais t'excuses pas, j'm'en fous c'est pour toi&#160;!</span></p>
</div>
<ul>
<li>
<div class="ref">Alban Lenoir, Simon Astier, <i>Hero Corp</i>, saison 1, épisode 10 (<i>L'alerte</i>), écrit par Simon Astier, première diffusion par Comédie&#160;!.</div>
</li>
</ul>
<p><br /></p>
<h3><span class="mw-headline" id=".C3.89pisode_11_:_Chez_l.27habitant">Épisode 11&#160;: Chez l'habitant</span></h3>
<div class="poem">
<p><span class="citation"><span class="personnage">Jennifer</span>&#160;: Vous bossez sur quoi?<br />
<span class="personnage">John</span>&#160;: Euh un truc...<br />
<span class="personnage">Doug</span>&#160;: Secret.<br />
<span class="personnage">Klaus</span>&#160;: Jambon.<br />
<span class="personnage">John</span>&#160;: Euh ouais un truc secret sur du jambon. (Doug se raidit)</span></p>
</div>
<ul>
<li>
<div class="ref">Aurore Pourteyron , Simon Astier, Sébastien Lalanne, Alban Lenoir, <i>Hero Corp</i>, saison 1, épisode 11 (<i>Chez l'habitant</i>), écrit par Simon Astier, première diffusion par Comédie&#160;!.</div>
</li>
</ul>
<p><br />
<span class="personnage">John</span>&#160;: You....to....to go.... to go to... hein?! Good bye!</p>
<ul>
<li>
<div class="ref">Simon Astier, <i>Hero Corp</i>, saison 1, épisode 11 (<i>Chez l'habitant</i>), écrit par Simon Astier, première diffusion par Comédie&#160;!.</div>
</li>
</ul>
<p><br /></p>
<h3><span class="mw-headline" id=".C3.89pisode_12_:_Nouvelle_peau">Épisode 12&#160;: Nouvelle peau</span></h3>
<div class="poem">
<p><span class="citation"><span class="personnage">Chauve Souris Man</span>&#160;: Je vis la nuit.<br />
<span class="personnage">John</span>&#160;: Mais là on est le jour...<br />
<span class="personnage">Chauve Souris Man</span>&#160;: Oui, mais je vis aussi le jour.<br />
<span class="personnage">John</span>&#160;: Mais vous dormez quand&#160;?<br />
<span class="personnage">Chauve Souris Man</span>&#160;: Ben, la nuit comme tout le monde.<br />
<span class="personnage">John</span>&#160;: Mais je croyais que vous viviez la nuit&#160;!<br />
<span class="personnage">Chauve Souris Man</span>&#160;: Ben pas toute la nuit non plus&#160;!</span></p>
</div>
<ul>
<li>
<div class="ref">Jonathan Lambert , Simon Astier, <i>Hero Corp</i>, saison 1, épisode 12 (<i>Nouvelle peau</i>), écrit par Simon Astier, première diffusion par Comédie&#160;!.</div>
</li>
</ul>
<p><br /></p>
<h3><span class="mw-headline" id=".C3.89pisode_13_:_Die_hard">Épisode 13&#160;: Die hard</span></h3>
<div class="poem">
<p><span class="citation"><span class="personnage">Captain Sports Extrêmes</span>&#160;: Bon, on a trois possibilités. Soit on fait nos connasses et on descend en rappel, soit on n'est pas des fiottes et on y va en free fly&#160;!<br />
<span class="personnage">John</span>&#160;: Ouais donc y a que deux possibilités en fait.</span></p>
</div>
<ul>
<li>
<div class="ref">Arnaud Tsamère, Simon Astier, <i>Hero Corp</i>, saison 1, épisode 13 (<i>Die hard</i>), écrit par Simon Astier, première diffusion par Comédie&#160;!.</div>
</li>
</ul>
<p><br /></p>
<div class="poem">
<p><span class="citation"><span class="personnage">Captain Sports Extrêmes</span>&#160;: Ah comment j'ai trop géré ma mère&#160;!</span></p>
</div>
<ul>
<li>
<div class="ref">Arnaud Tsamère, <i>Hero Corp</i>, saison 1, épisode 13 (<i>Die hard</i>), écrit par Simon Astier, première diffusion par Comédie&#160;!.</div>
</li>
</ul>
<p><br /></p>
<div class="poem">
<p><span class="citation"><span class="personnage">Mique</span>&#160;: Vous savez, John et moi, on est un peu comme frère et sœur&#160;!<br />
<span class="personnage">Laurence</span>&#160;: La sœur, c'est vous, non&#160;?</span></p>
</div>
<ul>
<li>
<div class="ref">Étienne Fague, (voix de) Didier Benureau, <i>Hero Corp</i>, saison 1, épisode 13 (<i>Die hard</i>), écrit par Simon Astier, première diffusion par Comédie&#160;!.</div>
</li>
</ul>
<p><br /></p>
<div class="poem">
<p><span class="citation"><span class="personnage">Laurence</span>&#160;: Ben vous êtes pas Mique&#160;?<br />
<span class="personnage">Mique</span>: Si&#160;!<br />
<span class="personnage">Laurence</span>: C'est pas vous qu'il appelle la tête de cul&#160;?<br />
<span class="personnage">Mique</span>: C'est possible...</span></p>
</div>
<ul>
<li>
<div class="ref">Étienne Fague, (voix de) Didier Benureau, <i>Hero Corp</i>, saison 1, épisode 13 (<i>Die hard</i>), écrit par Simon Astier, première diffusion par Comédie&#160;!.</div>
</li>
</ul>
<p><br /></p>
<h3><span class="mw-headline" id=".C3.89pisode_14_:_Duel">Épisode 14&#160;: Duel</span></h3>
<div class="poem">
<p><span class="citation"><span class="personnage">John</span>&#160;: Je voulais te dire merci pour tout, pour ce que tu as fais depuis le début, tu m'as soutenu, tout ça....<br />
<span class="personnage">Klaus</span>&#160;: Tu veux me pécho c'est ça?</span></p>
</div>
<ul>
<li>
<div class="ref">Alban Lenoir, Simon Astier, <i>Hero Corp</i>, saison 1, épisode 14 (<i>Duel</i>), écrit par Simon Astier, première diffusion par Comédie&#160;!.</div>
</li>
</ul>
<p><br /></p>
<div class="poem">
<p><span class="citation"><span class="personnage">John</span>&#160;: Qu'est ce qui se passe&#160;?<br />
<span class="personnage">Klaus</span>&#160;: Je sais pas mais il y a vraiment une ambiance de merde.<br />
(The Lord arrive)<br />
<span class="personnage">The Lord</span>&#160;: Bonjour John.<br />
<span class="personnage">John</span>&#160;: Et merde.<br />
<span class="personnage">Klaus</span>&#160;: Ah ouais merde, ouais. (parlant a John) Bon t'as toujours rien là&#160;?<br />
<span class="personnage">John</span>&#160;: J'ai super envie de pisser je sais pas si ça compte.<br />
<span class="personnage">Klaus</span>&#160;: Je pense pas.<br />
<span class="personnage">John</span>&#160;: (parlant à <span class="personnage">The Lord</span>) Attendez! on peut discuter, non&#160;?<br />
<span class="personnage">The Lord</span>&#160;: De quoi&#160;?<br />
<span class="personnage">John</span>&#160;: Ben je sais pas mais on pourrait en parler.<br />
<span class="personnage">The Lord</span>&#160;: Mais de quoi&#160;?<br />
<span class="personnage">John</span>&#160;: Moi je pense que ce serait bien qu'on en parle.<br />
<span class="personnage">The Lord</span>&#160;: Ouais, bon.</span></p>
</div>
<ul>
<li>
<div class="ref">Simon Astier, Alban Lenoir, Christian Bujeau, <i>Hero Corp</i>, saison 1, épisode 14 (<i>Duel</i>), écrit par Simon Astier, première diffusion par Comédie&#160;!.</div>
</li>
</ul>
<p><br /></p>
<h3><span class="mw-headline" id=".C3.89pisode_15_:_Apr.C3.A8s_le_calme">Épisode 15&#160;: Après le calme</span></h3>
<div class="poem">
<p><span class="citation"><span class="personnage">Mary</span>&#160;: Mes amis, mes amis, c'est un grand jour, mon neveu nous a sauvé la vie, il a vaincu The Lord, et nous a délivré de son emprise maléfique&#160;!<br />
<span class="personnage">Stan</span>&#160;: Ouais, moi j'trouve ça un tantinet sur-évalué.</span></p>
</div>
<ul>
<li>
<div class="ref">Agnès Boury, Arnaud Joyet, <i>Hero Corp</i>, saison 1, épisode 15 (<i>Après le calme</i>), écrit par Simon Astier, première diffusion par Comédie&#160;!.</div>
</li>
</ul>
<p><br /></p>
<div class="poem">
<p><span class="citation"><span class="personnage">Mac Kormack</span>&#160;: Il possède des informations que nous devons absolument récupérer, et vous allez nous aider. L'avenir du monde en dépend.<br />
<span class="personnage">John</span>&#160;: Ah carrément&#160;?<br />
<span class="personnage">Mac Kormack</span>&#160;: Oui. Carrément.</span></p>
</div>
<ul>
<li>
<div class="ref">Lionnel Astier, Simon Astier, <i>Hero Corp</i>, saison 1, épisode 15 (<i>Après le calme</i>), écrit par Simon Astier, première diffusion par Comédie&#160;!.</div>
</li>
</ul>
<p><br /></p>
<div class="poem">
<p><span class="citation"><span class="personnage">John</span>&#160;: Vous me demandez de sauver le monde, ça fait deux fois dans l'année, je trouve ça déjà un peu beaucoup, et de trahir ma copine, j'me trouve assez diplomate là.<br />
<span class="personnage">Mac Kormack</span>&#160;: Oui c'est sûr que comme ça, ça donne pas très envie.</span></p>
</div>
<ul>
<li>
<div class="ref">Simon Astier, Lionnel Astier, <i>Hero Corp</i>, saison 1, épisode 15 (<i>Après le calme</i>), écrit par Simon Astier, première diffusion par Comédie&#160;!.</div>
</li>
</ul>
<p><br /></p>
<div class="poem">
<p><span class="citation"><span class="personnage">John</span>&#160;: Mais qu'est-ce que vous foutez là&#160;?<br />
<span class="personnage">Doug</span>&#160;: T'es content de nous voir&#160;?<br />
<span class="personnage">John</span>&#160;: Mais qu'est-ce que vous foutez là&#160;?<br />
<span class="personnage">Doug</span>&#160;: Mais t'es content ou pas&#160;?<br />
<span class="personnage">John</span>&#160;: J'comprends plus rien moi.<br />
<span class="personnage">Klaus</span>&#160;: Ouais t'es pas content de nous voir quoi.</span></p>
</div>
<ul>
<li>
<div class="ref">Simon Astier, Sébastien Lalanne, Alban Lenoir, <i>Hero Corp</i>, saison 1, épisode 15 (<i>Après le calme</i>), écrit par Simon Astier, première diffusion par Comédie&#160;!.</div>
</li>
</ul>
<p><br /></p>
<div class="poem">
<p><span class="citation"><span class="personnage">Doug</span>&#160;: Qu'est-ce qu'ils te veulent&#160;?<br />
<span class="personnage">John</span>&#160;: En gros ils veulent que je sauve le monde. Comme d'hab quoi.<br />
<span class="personnage">Klaus</span>&#160;: Ils s'font pas chier les mecs quand même.<br />
<span class="personnage">Doug</span>&#160;: Nan mais c'est classe&#160;! Moi on m'a jamais demandé de sauver le monde.<br />
<span class="personnage">Klaus</span>&#160;: C'est parce que t'es trop petit. En dessous d'1m80 t'es niqué.</span></p>
</div>
<ul>
<li>
<div class="ref">Sébastien Lalanne, Simon Astier, Alban Lenoir, <i>Hero Corp</i>, saison 1, épisode 15 (<i>Après le calme</i>), écrit par Simon Astier, première diffusion par Comédie&#160;!.</div>
</li>
</ul>
<p><br /></p>
<div class="poem">
<p><span class="citation"><span class="personnage">Lawrence</span>&#160;: Eh les gars, ça vous dirait pas d'aller faire un tour&#160;? Ce serait chouettos&#160;!<br />
(silence)<br />
<span class="personnage">Doug</span>&#160;: Ca se dit encore chouettos&#160;?<br />
<span class="personnage">John</span>&#160;: J'suis pas sûr.</span></p>
</div>
<ul>
<li>
<div class="ref">Didier Bénureau, Sébastien Lalanne, Simon Astier, <i>Hero Corp</i>, saison 1, épisode 15 (<i>Après le calme</i>), écrit par Simon Astier, première diffusion par Comédie&#160;!.</div>
</li>
</ul>
<p><br /></p>
<h2><span class="mw-headline" id="Saison_2">Saison 2</span></h2>
<h3><span class="mw-headline" id=".C3.89pisode_1_:_La_temp.C3.AAte">Épisode 1&#160;: La tempête</span></h3>
<div class="poem">
<p><span class="citation"><span class="personnage">Klaus</span>&#160;: Tu veux un coup de main&#160;?<br />
<span class="personnage">John</span>&#160;: Oh ouais.<br />
<span class="personnage">Klaus</span>&#160;: Oh merde.<br />
<span class="personnage">John</span>&#160;: Bah propose pas si ça t'fait chier sinon&#160;!</span></p>
</div>
<ul>
<li>
<div class="ref">Alban Lenoir, Simon Astier, <i>Hero Corp</i>, saison 2, épisode 1 (<i>La tempête</i>), écrit par Simon Astier, première diffusion par Comédie&#160;!.</div>
</li>
</ul>
<p><br /></p>
<div class="poem">
<p><span class="citation"><span class="personnage">Klaus</span>&#160;: (sanglotant) Et bah voilà, ça y est, on pleure comme des petites connasses.<br />
<span class="personnage">Doug</span>&#160;: Ah non mais je pleure pas moi.<br />
(Doug se raidit)</span></p>
</div>
<ul>
<li>
<div class="ref">Alban Lenoir, Sébastien Lalanne, <i>Hero Corp</i>, saison 2, épisode 1 (<i>La tempête</i>), écrit par Simon Astier, première diffusion par Comédie&#160;!.</div>
</li>
</ul>
<p><br /></p>
<div class="poem">
<p><span class="citation">(Klaus, Doug et John sanglotent)<br />
<span class="personnage">Jennifer</span>&#160;: Eh les filles, quand vous aurez fini de vous tripoter y'en a bien une qui m'amènera un truc à bouffer&#160;?</span></p>
</div>
<ul>
<li>
<div class="ref">Aurore Pourteyron, <i>Hero Corp</i>, saison 2, épisode 1 (<i>La tempête</i>), écrit par Simon Astier, première diffusion par Comédie&#160;!.</div>
</li>
</ul>
<p><br /></p>
<div class="poem">
<p><span class="citation">(Klaus, Doug et John atterrissent après avoir été transportés par Captain Canada)<br />
<span class="personnage">Klaus</span>&#160;: Il est où Doug&#160;?<br />
<span class="personnage">Doug</span> &#160;: [Criant après être tombé d’un arbre] EH BAH VOILA COMME PAR HASARD HEIN&#160;! Y EN A QU’UN SEUL QUI RESTE COINCÉ EN HAUT D’UN ARBRE et c’est moi, comme par hasard… [«&#160;Pleurant&#160;»]</span></p>
</div>
<ul>
<li>
<div class="ref">Sébastien Lalanne, <i>Hero Corp</i>, saison 2, épisode 1 (<i>La tempête</i>), écrit par Simon Astier, première diffusion par Comédie&#160;!.</div>
</li>
</ul>
<p><br /></p>
<h3><span class="mw-headline" id=".C3.89pisode_3_:_Ex-.C3.A6quo">Épisode 3&#160;: Ex-æquo</span></h3>
<div class="poem">
<p><span class="citation"><span class="personnage">Neil Mac Kormack</span>&#160;: Vous avez pas de vision sur le commanditaire&#160;? Ou une vision tout court d'ailleurs, parce que ça commence a faire long, là.<br />
<span class="personnage">Théodore</span>&#160;: Ben c'est-à-dire que j'ai un peu faillit décéder violemment. Du coup je vais laisser mon cerveau se réoxygéner, et je vous tiens au courant.</span></p>
</div>
<ul>
<li>
<div class="ref">Lionnel Astier, Jacques Fontanel, <i>Hero Corp</i>, saison 2, épisode 3 (<i>Ex-æquo</i>), écrit par Simon Astier, première diffusion par Comédie&#160;!.</div>
</li>
</ul>
<p><br /></p>
<div class="poem">
<p><span class="citation"><span class="personnage">La fille en maillot de bain</span>&#160;: Merci beaucoup&#160;!<br />
<span class="personnage">Klaus</span>&#160;: Franchement c'était tranquille.<br />
<span class="personnage">La fille en maillot de bain</span>&#160;: J'peux vous offrir un café pour vous remercier&#160;? J'habite à côté.<br />
<span class="personnage">Klaus</span> (à <span class="personnage">Doug</span>)&#160;: Tu viens&#160;? (Silence)<br />
<span class="personnage">La fille en maillot de bain</span>&#160;: C'est bon, c'est juste un café, je vais pas vous séquestrer. (Doug se raidit)</span></p>
</div>
<ul>
<li>
<div class="ref">Alban Lenoir, <i>Hero Corp</i>, saison 2, épisode 3 (<i>Ex-æquo</i>), écrit par Simon Astier, première diffusion par Comédie&#160;!.</div>
</li>
</ul>
<p><br /></p>
<div class="poem">
<p><span class="citation">&#160;<span class="personnage">Doug</span>&#160;: "On dira rien, ON DIRA RIEN!!!!!"<br />
&#160;<span class="personnage">Klaus</span>&#160;: "Ouai on dira rien!!"<br />
&#160;<span class="personnage">Homme HWK</span>&#160;: "Ils sont où les autres?"<br />
&#160;<span class="personnage">Doug</span>&#160;: "MERDE!!!!!"<br />
&#160;<span class="personnage">Homme HWK</span>&#160;: "ILS SONT OU LES AUTRES!!!??"<br />
&#160;<span class="personnage">Doug</span>&#160;: "Merde."<br />
&#160;<span class="personnage">Homme HWK</span>&#160;: "J'vais pas l'répéter une 3ème fois"<br />
&#160;<span class="personnage">Doug</span>&#160;: "Zgeg!!"<br />
&#160;<span class="personnage">Klaus</span>&#160;: "C'est pas la peine de vous énerver on dira rien on vous dit!"<br />
&#160;<span class="personnage">Homme HWK</span>&#160;: "Pour l'instant, on fait ça dans une bonne ambiance, mais j'peux aller chercher les outils si vous voulez!"<br />
&#160;<span class="personnage">Doug</span>&#160;: "PINE!!!!!"<br />
&#160;[...]<br />
&#160;<span class="personnage">Doug/​Klaus</span>&#160;: "NON ATTENDEZ-ATTENDEZ-ATTENDEZ-ATTENDEZ-​ATTENDEZ-ATTENDEZ-ATTENDEZ-ATT​ENDEZ-ATTENDEZ!! NON-NON-NON-NON-NON-NON-NON-NON!!!!!!!!!"<br />
&#160;<span class="personnage">Klaus</span>&#160;: "Vous voyez les sous-bois derrière les chataîgners à 500m au nord?"<br />
&#160;<span class="personnage">Homme HWK</span>&#160;: "Ouai....?"<br />
&#160;<span class="personnage">Klaus</span>&#160;: "Oooh ouai ben c'est pas du tout là..."<br />
&#160;<span class="personnage">Homme HWK</span>&#160;: "C'est votre dernier mot??"<br />
&#160;<span class="personnage">Doug</span>&#160;: "Heu non!"<br />
&#160;<span class="personnage">Homme HWK</span>&#160;: "Tain.. j'savais qu'tu parlerais toi.."<br />
&#160;<span class="personnage">Doug</span>&#160;: "Ouai, moi j'voulais vous dire .. FESSE."<br />
&#160;[...]<br />
&#160;<span class="personnage">Doug/​Klaus</span>&#160;: "NON ATTENDEZ-ATTENDEZ-ATTENDEZ-ATTENDEZ.​....!!!!!!!!!!!!!!!!!!"</span></p>
</div>
<ul>
<li>
<div class="ref">Alban Lenoir , Sebastien Lalanne, <i>Hero Corp</i>, saison 2, épisode 3 (<i>Ex-æquo</i>), écrit par Simon Astier, première diffusion par Comédie&#160;!.</div>
</li>
</ul>
<p><br /></p>
<div class="poem">
<p><span class="citation"><span class="personnage">Mary</span>&#160;: Même si aux premiers abords ça sautait pas aux yeux, elle était très jolie votre histoire. Quand tu l'as en face de toi, tu ressens vraiment rien&#160;?<br />
<span class="personnage">Jennifer</span>&#160;: Non.<br />
<span class="personnage">Mary</span>&#160;: Quand il te regarde, non plus&#160;?<br />
<span class="personnage">Jennifer</span>&#160;: Pfff... Ouais... A la limite, il pourrait faire un petit quatrième, quoi. Ouais parce que j'ai fait une liste.<br />
<span class="personnage">Mary</span>&#160;: Ouais, j'suis au courant.<br />
<span class="personnage">Jennifer</span>&#160;: Euh non, bah non, euh troisième du coup, parce que quand j'ai fait la liste je pensais encore que vous étiez un homme.</span></p>
</div>
<ul>
<li>
<div class="ref">Agnès Boury, Aurore Pourteyron, <i>Hero Corp</i>, saison 2, épisode 3 (<i>Ex-æquo</i>), écrit par Simon Astier, première diffusion par Comédie&#160;!.</div>
</li>
</ul>
<p><br /></p>
<h3><span class="mw-headline" id=".C3.89pisode_13_:_Instructions">Épisode 13&#160;: Instructions</span></h3>
<div class="poem">
<p><span class="citation"><span class="personnage">Burt</span>&#160;: T'es un vrai poltron mon p'tit vieux&#160;!<br />
<span class="personnage">Stan</span>&#160;: Qu'est-ce que t'as dit&#160;?<br />
<span class="personnage">Burt</span>&#160;: J'ai dit que t'étais un vrai poltron&#160;!<br />
<span class="personnage">Stan</span>&#160;: Oh la vache&#160;! J'avais carrément oublié ce mot, j'crois&#160;!<br />
<span class="personnage">Burt</span>&#160;: On peut faire ce qu'on a à faire ou on continue à tergiverser jusqu'à la Saint Glinglin&#160;?<br />
<span class="personnage">Stan</span>&#160;: Oh non mais là tergiverser j'avais oublié aussi, mais Saint Glinglin j'ai jamais connu j'crois.</span></p>
</div>
<ul>
<li>
<div class="ref">François Podetti, Arnaud Joyet, <i>Hero Corp</i>, saison 2, épisode 13 (<i>Instructions</i>), écrit par Simon Astier.</div>
</li>
</ul>
<p><br /></p>
<h3><span class="mw-headline" id=".C3.89pisode_14_:_Une_nouvelle_.C3.A8re_1.2F2">Épisode 14&#160;: Une nouvelle ère 1/2</span></h3>
<div class="poem">
<p><span class="citation"><span class="personnage">John</span>&#160;: Ça va&#160;?<br />
<span class="personnage">Jennifer</span>&#160;: Touche à ton cul, sinon...</span></p>
</div>
<ul>
<li>
<div class="ref">Simon Astier et Aurore Pourteyron, <i>Hero Corp</i>, saison 2, épisode 14 (<i>Une nouvelle ère 1/2</i>), écrit par Simon Astier.</div>
</li>
</ul>
<p><br /></p>
<p><span class="citation"><span class="personnage">Mique</span> (à <span class="personnage">John</span>)&#160;: L’autre la honte&#160;! Son beau-père c’est The Lord.</span></p>
<ul>
<li>
<div class="ref">Etienne Fague, <i>Hero Corp</i>, saison 2, épisode 14 (<i>Une nouvelle ère 1/2</i>)), écrit par Simon Astier.</div>
</li>
</ul>
<p><br /></p>
<p><span class="citation"><span class="personnage">Mary</span> (à propos de <span class="personnage">Valur</span>)&#160;: Je suis désolée pour ce que je vais dire, mais son accent ça me chauffe un peu le cul.</span></p>
<ul>
<li>
<div class="ref">Agnès Boury, <i>Hero Corp</i>, saison 2, épisode 14 (<i>Une nouvelle ère 1/2</i>), écrit par Simon Astier.</div>
</li>
</ul>
<p><br /></p>
<div class="poem">
<p><span class="citation"><span class="personnage">Doug</span>&#160;: On est mort&#160;! Je suis sûr qu'on est mort&#160;!<br />
<span class="personnage">Stan</span>&#160;: Mais non, quand on est mort on voit de la lumière.<br />
<span class="personnage">Klaus</span>&#160;: Parce que t'as déjà été mort toi&#160;?<br />
<span class="personnage">Stève</span>&#160;: C'est pas parce qu'on est dans le noir que je peux pas te latter ton cul&#160;!</span></p>
</div>
<ul>
<li>
<div class="ref">Sébastien Lalanne, Arnaud Joyet, Alban Lenoir et Gérard Darier, <i>Hero Corp</i>, saison 2, épisode 14 (<i>Une nouvelle ère 1/2</i>), écrit par Simon Astier.</div>
</li>
</ul>
<p><br /></p>
<div class="poem">
<p><span class="citation"><span class="personnage">Doug</span>&#160;: Ça y est, je sens mon corps partir, il s'allège comme une plume.<br />
<span class="personnage">Klaus</span>&#160;: Ouais, ben on est quand même sur de la grosse plume là&#160;!</span></p>
</div>
<ul>
<li>
<div class="ref">Sébastien Lalanne et Alban Lenoir, <i>Hero Corp</i>, saison 2, épisode 14 (<i>Une nouvelle ère 1/2</i>), écrit par Simon Astier.</div>
</li>
</ul>
<p><br /></p>
<p><span class="citation"><span class="personnage">Kyle</span>&#160;: Moi je vais prendre une mousse. Quand je suis saoul, je lâche des caisses hyper sonores. On va se marrer.</span></p>
<ul>
<li>
<div class="ref">François Frappier, <i>Hero Corp</i>, saison 2, épisode 14 (<i>Une nouvelle ère 1/2</i>), écrit par Simon Astier.</div>
</li>
</ul>
<p><br /></p>
<p><span class="citation"><span class="personnage">Klaus</span>&#160;: Moi, si on commence à jeter des animaux décédés, franchement ça va pas me le faire.</span></p>
<ul>
<li>
<div class="ref">Alban Lenoir, <i>Hero Corp</i>, saison 2, épisode 14 (<i>Une nouvelle ère 1/2</i>), écrit par Simon Astier.</div>
</li>
</ul>
<p><br /></p>
<p><span class="citation"><span class="personnage">John Senior</span> (saoul)&#160;: Sur Saint-Denis je chante ma vie, sur Sainte-Catherine je montre ma pine&#160;!</span></p>
<ul>
<li>
<div class="ref">Jacques Ville, <i>Hero Corp</i>, saison 2, épisode 14 (<i>Une nouvelle ère 1/2</i>), écrit par Simon Astier.</div>
</li>
</ul>
<p><br /></p>
<div class="poem">
<p><span class="citation"><span class="personnage">Klaus</span>&#160;: C'est bon, y'en a un qui arrive.<br />
<span class="personnage">Stève</span>&#160;: On y va&#160;?<br />
<span class="personnage">Klaus</span>&#160;: j'ai dis y'en a UN qui arrive.<br />
<span class="personnage">Stève</span>&#160;: Et ben alors&#160;?<br />
<span class="personnage">Klaus</span>&#160;: Ben, en-dessous de huit j'y vais tout seul. Je vois même pas pourquoi je te le rappelle.<br />
<span class="personnage">Stève</span>&#160;: Oui c'est vrai, je suis con.</span></p>
</div>
<ul>
<li>
<div class="ref">Alban Lenoir et Gérard Darier, <i>Hero Corp</i>, saison 2, épisode 14 (<i>Une nouvelle ère 1/2</i>), écrit par Simon Astier.</div>
</li>
</ul>
<p><br /></p>
<div class="poem">
<p><span class="citation">(Klaus, à un personnage retourné)<br />
<span class="personnage">Klaus</span>&#160;: Hé machin&#160;!<br />
<span class="personnage">Inconnu</span>&#160;: *pas de réponse*<br />
<span class="personnage">Klaus</span>&#160;: Bon, bah pinage alors...</span></p>
</div>
<ul>
<li>
<div class="ref">Alban Lenoir, <i>Hero Corp</i>, saison 2, épisode 14 (<i>Une nouvelle ère 1/2</i>), écrit par Simon Astier, première diffusion par Comédie&#160;!.</div>
</li>
</ul>
<p><br /></p>
<h3><span class="mw-headline" id=".C3.89pisode_15_:_Une_nouvelle_.C3.A8re_2.2F2">Épisode 15&#160;: Une nouvelle ère 2/2</span></h3>
<div class="poem">
<p><span class="citation"><span class="personnage">Neil Mac Kormack</span> (Qui a réussi à se téléporter )&#160;: Ha la vache&#160;! J'ai réussi&#160;!<br />
<span class="personnage">Mique</span>&#160;: Ben allez chercher les coupes, je m'occupe du champagne&#160;!</span></p>
</div>
<ul>
<li>
<div class="ref">Lionnel Astier et Etienne Fague, <i>Hero Corp</i>, saison 2, épisode 15 (<i>Une nouvelle ère 2/2</i>), écrit par Simon Astier.</div>
</li>
</ul>
<p><br /></p>
<div class="poem">
<p><span class="citation"><span class="personnage">Captain Sports Extrêmes</span>&#160;: Je crois qu'on est sur une aire ou y'a des mecs qui aiment bien les sensass'. Je crois qu'on est sur une aire où quand t'es une petite connasse, il vaut mieux faire demi tour. Je crois qu'on est sur une aire de paintball.<br />
<span class="personnage">Jean Micheng</span>&#160;: Donc c'est normal que j'aie pas mal.<br />
<span class="personnage">Captain Sports Extrêmes</span>&#160;: Du coup ouais.</span></p>
</div>
<ul>
<li>
<div class="ref">Arnaud Tsamère et Patrick Vo, <i>Hero Corp</i>, saison 2, épisode 15 (<i>Une nouvelle ère 2/2</i>), écrit par Simon Astier.</div>
</li>
</ul>
<p><br /></p>
<div class="poem">
<p><span class="citation"><span class="personnage">Captain Sports Extrêmes</span>&#160;: Hey salut les mecs&#160;! Ça roule du cul ou quoi&#160;!</span></p>
</div>
<ul>
<li>
<div class="ref">Arnaud Tsamère, <i>Hero Corp</i>, saison 2, épisode 15 (<i>Une nouvelle ère 2/2</i>), écrit par Simon Astier.</div>
</li>
</ul>
<p><br /></p>
<div class="poem">
<p><span class="citation"><span class="personnage">Klaus</span>&#160;: A ben non mais partez pas&#160;! C'est pas parce que vous êtes des faux vampires qu'on va pas vous dérouiller vos gueules.</span></p>
</div>
<ul>
<li>
<div class="ref">Alban Lenoir, <i>Hero Corp</i>, saison 2, épisode 15 (<i>Une nouvelle ère 2/2</i>), écrit par Simon Astier.</div>
</li>
</ul>
<p><br /></p>
<div class="poem">
<p><span class="citation"><span class="personnage">Doug</span>&#160;: Juste, je peux savoir pourquoi j'ai une cabine qui sent le vieux slibard&#160;?<br />
<span class="personnage">Dan</span>&#160;: Mais Tous les trucs en rapport avec la mer ça sent le vieux slibard. <i>(Lui tendant le bras pour qu'il le sente)</i> Tiens, moi, regarde&#160;!<br />
<span class="personnage">Doug</span>&#160;: Mais je suis en dessous du niveau de l'eau alors je dors sous l'eau quoi&#160;! Je suis trop la troisième roue du bateau&#160;!</span></p>
</div>
<ul>
<li>
<div class="ref">Sébastien Lalanne et Jean-Luc Couchard, <i>Hero Corp</i>, saison 2, épisode 15 (<i>Une nouvelle ère 2/2</i>), écrit par Simon Astier.</div>
</li>
</ul>
<p><br /></p>
<div class="poem">
<p><span class="citation"><span class="personnage">Klaus</span>&#160;: Ah ouais, mais c’est carrément des vampires, en fait&#160;!<br />
<span class="personnage">Doug</span>&#160;: Ah mais non, quand même... AH SI LA VACHE&#160;!</span></p>
</div>
<ul>
<li>
<div class="ref">Alban Lenoir et Sébastien Lalanne, <i>Hero Corp</i>, saison 2, épisode 15 (<i>Une nouvelle ère 2/2</i>), écrit par Simon Astier.</div>
</li>
</ul>
<p><br /></p>
<div class="noprint" style="clear:right;float:right;margin:1em 0 1em 1em;width:250px;border:solid #aaaaaa 1px;background:#f9f9f9;padding:4px;font-size:90%;text-align:left;">
<p>Vous pouvez également consulter les articles suivants sur les autres projets <a href="https://meta.wikimedia.org/wiki/Accueil" class="extiw" title="m:Accueil">Wikimédia</a>&#160;:</p>
<div style="clear:left;padding:1px">
<div style="float:left;margin:1px">
<div style="position:relative;width:25px;height:30px;z-index:2;overflow:hidden;"><a href="/wiki/Fichier:Wikipedia-logo.png" class="image"><img alt="Wikipedia-logo.png" src="//upload.wikimedia.org/wikipedia/commons/thumb/6/63/Wikipedia-logo.png/25px-Wikipedia-logo.png" width="25" height="25" srcset="//upload.wikimedia.org/wikipedia/commons/thumb/6/63/Wikipedia-logo.png/38px-Wikipedia-logo.png 1.5x, //upload.wikimedia.org/wikipedia/commons/thumb/6/63/Wikipedia-logo.png/50px-Wikipedia-logo.png 2x" data-file-width="200" data-file-height="200" /></a>
<div class="nodeco" style="position:absolute;top:0;left:0;padding-top:3px;z-index:3;"><a href="https://fr.wikipedia.org/wiki/Hero_Corp" class="extiw" title="w:Hero Corp"><span style="float:left;width:25px;height:30px;font-size:30px;line-height:30px;word-spacing:25px;cursor:hand;">&#160; &#160;</span></a></div>
</div>
</div>
<p><i><b><a href="https://fr.wikipedia.org/wiki/Hero_Corp" class="extiw" title="w:Hero Corp">Article</a></b></i> sur <a href="https://fr.wikipedia.org/wiki/" class="extiw" title="w:">Wikipédia</a>.</p>
</div>
</div>


<!-- 
NewPP limit report
Parsed by mw1262
Cached time: 20170530135345
Cache expiry: 2592000
Dynamic content: false
CPU time usage: 0.176 seconds
Real time usage: 0.195 seconds
Preprocessor visited node count: 9599/1000000
Preprocessor generated node count: 0/1500000
Post‐expand include size: 26214/2097152 bytes
Template argument size: 16499/2097152 bytes
Highest expansion depth: 11/40
Expensive parser function count: 0/500
Lua time usage: 0.003/10.000 seconds
Lua memory usage: 558 KB/50 MB
-->
<!--
Transclusion expansion time report (%,ms,calls,template)
100.00%  148.592      1 -total
 39.84%   59.201     58 Modèle:Réf_Série
 37.84%   56.223     57 Modèle:Citation
 30.69%   45.603     58 Modèle:Réf_imprécise
 27.24%   40.481     58 Modèle:Print
 22.20%   32.993     58 Modèle:Concat10
 12.15%   18.061      1 Modèle:Titre_en_italique
  3.86%    5.737    202 Modèle:Personnage
  2.51%    3.723      1 Modèle:Autres_projets
  2.15%    3.199      3 Modèle:Date
-->
</div>
<!-- Saved in parser cache with key frwikiquote:pcache:idhash:12959-0!*!0!!fr!4!* and timestamp 20170530135345 and revision id 254620
 -->
`


export default expected_fr_hero_corp_html
