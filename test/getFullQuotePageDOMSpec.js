import '../polyfill/ElementMatches'


import chai from 'chai'
chai.should()

import fetchMocked from 'fetch-mock'


import isFrozenEnvironment from './isFrozenEnvironment'
import fakeResponse from './fakeResponse'


import getFullQuotePageDOM from '../src/getFullQuotePageDOM'
import WikiquoteApi from '../src/WikiquoteApi'



describe('Wikiquote API get full quote page DOM', function() {
	this.timeout(isFrozenEnvironment ? 1000 : 10000)

	if (isFrozenEnvironment) {
		beforeEach(() => {
			fetchMocked.getOnce(/fr\.wikiquote\.org.+&search=Kaamelott$/, (url, option) => [
				'',
				[
					'Kaamelott',
					'Kaamelott/Yvain',
				],
			])

			for (const [ urlPattern, fileContent ] of fakeResponse) {
				fetchMocked.get(urlPattern, fileContent)
			}
		})

		afterEach(fetchMocked.restore)
	}



	it('should return an empty Map if there are an empty search term', async () => {
		const emptyQuotePage = await getFullQuotePageDOM()
		emptyQuotePage.should.be.a('Map').empty
	})


	it('should return the DOM of the quotes pages', async () => {
		const quotePageMap = await getFullQuotePageDOM('Kaamelott', 'fr')
		quotePageMap.size.should.be.equal(isFrozenEnvironment ? 2 : 31)

		const mainUrl = 'https://fr.wikiquote.org/wiki/Kaamelott'
		quotePageMap.has(mainUrl).should.be.true

		const quotePageContentMain = quotePageMap.get(mainUrl)
		quotePageContentMain.matches('div:not(:empty)').should.be.true

		if (isFrozenEnvironment) {
			quotePageContentMain.querySelectorAll('*').should.have.lengthOf(1543)
		}
		else {
			quotePageContentMain.querySelectorAll('*').should.have.lengthOf.at.least(1500)
		}

		const secondUrl = 'https://fr.wikiquote.org/wiki/Kaamelott%2FYvain'
		quotePageMap.has(secondUrl).should.be.true

		const quotePageContentSecond = quotePageMap.get(secondUrl)
		quotePageContentSecond.matches('div:not(:empty)').should.be.true

		if (isFrozenEnvironment) {
			quotePageContentSecond.querySelectorAll('*').should.have.lengthOf(358)
		}
		else {
			quotePageContentSecond.querySelectorAll('*').should.have.lengthOf.at.least(300)
		}
	})


	it('should be fast enough to resolve all content', function() {
		const delay = isFrozenEnvironment ? 500 : 2000
		this.timeout(delay)

		return getFullQuotePageDOM('Kaamelott', 'fr')
	})
})
