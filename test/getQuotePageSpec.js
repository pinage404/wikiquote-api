import chai from 'chai'
chai.should()

import chaiAsPromised from 'chai-as-promised'
chai.use(chaiAsPromised)

import fetchMocked from 'fetch-mock'


import isFrozenEnvironment from './isFrozenEnvironment'
import fakeResponse from './fakeResponse'
import expected_fr_kaamelott_html from './expected/getQuotePage/fr_kaamelott_html'
import expected_fr_kaamelott_yvain_html from './expected/getQuotePage/fr_kaamelott_yvain_html'
import expected_fr_hero_corp_html from './expected/getQuotePage/fr_hero_corp_html'


import getQuotePage from '../src/getQuotePage'

import { UnsupportedLanguageException, NoSuchPageException, DisambiguationPageException } from '../src/exception'



describe('Wikiquote API get quote page', function() {
	this.timeout(isFrozenEnvironment ? 300 : 1000)

	if (isFrozenEnvironment) {
		beforeEach(() => {
			for (const [ urlPattern, fileContent ] of fakeResponse) {
				fetchMocked.get(urlPattern, fileContent)
			}
		})

		afterEach(fetchMocked.restore)
	}



	it('should return an empty string if there are an empty search term', async () => {
		const emptyQuotePage = await getQuotePage()
		emptyQuotePage.should.be.a('string').empty
	})


	it('should raise exception on unsupported language', () => {
		const unsupportedLang = getQuotePage('Kaamelott', 'zh')
		return unsupportedLang.should.be.rejectedWith(UnsupportedLanguageException, 'Unsupported language: “zh”')
	})


	describe('should call the good URL using parameters', () => {
		const dontCare = new Error('don t care')

		beforeEach(() => {
			fetchMocked.restore().getOnce('*', (url, option) => { throw dontCare })
		})

		afterEach(fetchMocked.restore)


		it('in english', () => {
			const checkUrl = getQuotePage('South Park', 'en')

			checkUrl.should.be.rejectedWith(dontCare)

			const url = 'https://en.wikiquote.org/w/api.php?origin=*&format=json&action=parse&prop=text|categories&disabletoc&page=South%20Park'
			const option = {
				method: 'GET',
				mode: 'cors',
				credentials: 'omit',
			}
			return fetchMocked.lastCall().should.be.deep.equal([url, option])
		})


		it('in french', () => {
			const checkUrl = getQuotePage('Kaamelott', 'fr')

			checkUrl.should.be.rejectedWith(dontCare)

			const url = 'https://fr.wikiquote.org/w/api.php?origin=*&format=json&action=parse&prop=text|categories&disabletoc&page=Kaamelott'
			const option = {
				method: 'GET',
				mode: 'cors',
				credentials: 'omit',
			}
			return fetchMocked.lastCall().should.be.deep.equal([url, option])
		})
	})


	it('should throw an exception if the page is not found', () => {
		const notExistingPage = getQuotePage('This_page_should_never_exist_or_this_test_will_fail', 'fr')
		return notExistingPage.should.be.rejectedWith(NoSuchPageException, 'No pages matched the title: “This_page_should_never_exist_or_this_test_will_fail”')
	})


	describe('should throw an exception if the search term can correspond to several pages', () => {
		it('in english', () => {
			const ambiguousPage = getQuotePage('Harry Potter', 'en')
			return ambiguousPage.should.be.rejectedWith(DisambiguationPageException, 'Disambiguation page: “Harry Potter”')
		})


		it('in french', () => {
			const ambiguousPage = getQuotePage('V pour Vendetta', 'fr')
			return ambiguousPage.should.be.rejectedWith(DisambiguationPageException, 'Disambiguation page: “V pour Vendetta”')
		})
	})


	describe('should return the HTML of the page', () => {
		describe('in french', () => {
			it('main page', async () => {
				const html = await getQuotePage('Kaamelott', 'fr')
				html.should.have.lengthOf.at.least(71000)

				if (isFrozenEnvironment) {
					html.should.be.deep.equal(expected_fr_kaamelott_html)
				}
			})


			it('secondary page', async () => {
				const html = await getQuotePage('Kaamelott/Yvain', 'fr')
				html.should.have.lengthOf.at.least(14000)

				if (isFrozenEnvironment) {
					html.should.be.deep.equal(expected_fr_kaamelott_yvain_html)
				}
			})


			it('alternative page', async () => {
				const html = await getQuotePage('Hero Corp', 'fr')
				html.should.have.lengthOf.at.least(42000)

				if (isFrozenEnvironment) {
					html.should.be.deep.equal(expected_fr_hero_corp_html)
				}
			})
		})
	})
})
